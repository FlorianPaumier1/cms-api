<?= "<?php\n" ?>

namespace App\Form\Frontend;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

<?php foreach ($usesType as  $useType): ?>
use <?= $useType ?>;
<?php endforeach; ?>


class <?= $class_name ?> extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $option): FormBuilderInterface
    {
        return $builder
        <?php foreach ($form_fields as $form_field => $typeOptions): ?>
                ->add('<?= $form_field ?>', <?= $typeOptions["type"] ?> , <?= var_export($typeOptions["parameters"]) ?>)
        <?php endforeach; ?>
        ;
    }

}
