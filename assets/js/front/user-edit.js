let profilePictureInput = '';
$(document).ready(function () {
    profilePictureInput = $('#' + $('#edit-profile-picture').data('form-id'));
    profilePictureInput.change(function () {
        readURL(this);
    });
});

$('#edit-profile-picture').on('click', function () {
    profilePictureInput.click();
});

function readURL(input) {
    if (input.files && input.files[0]) {
        let reader = new FileReader();

        reader.onload = function (e) {
            $('#user-picture-preview').css('background-image', 'url(' + e.target.result + ')');
            $('#user-picture-preview').css('background-size', 'cover');
        };

        reader.readAsDataURL(input.files[0]);
    }
}


