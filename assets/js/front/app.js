const $ = require('jquery');
global.$ = global.jQuery = $;

require('bootstrap');
require('hover.css/css/hover-min.css');
require('jquery.scrollto/jquery.scrollTo.min');
require('selectize/dist/js/selectize.min');
require('../../css/front/app.scss');

$('select').each(function (index) {
    if ($(this).prop('multiple')) {
        $(this).selectize({
            plugins: ['remove_button']
        });
    } else {
        $(this).selectize({});
    }
});

$('#searchButton').on('click', function () {
    let $button = $(this);
    $button.prop('disabled', true);
    $('#searchLoader').removeClass('d-none');
    setTimeout(function () {
        $button.prop('disabled', false);
        $('#searchLoader').addClass('d-none');
        $('#results').removeClass('d-none');
        $(window).scrollTo($('#results'), 400, {offset: {top: -30}});
    }, 1000)
});

// $(document).ready(function () {
//     $('.loading-button').on('click', function () {
//         let $button = $(this);
//         $button.append('<i class="ml-1 far fa-spinner fa-spin"></i>')
//         setTimeout(function () {
//             $button.children('i.far').remove;
//         }, 100);
//     });
// });
