let sidebarSwitch = false;

$('#sidebarToggleButton').on('click', function () {
   var $sidebar = $('.sidebar');
   var $main = $('.main');
   var $overlay = $('.sidebar-backdrop');

   $sidebar.toggleClass('active');
   $main.toggleClass('sidebar-on');
   $overlay.toggleClass('active');

   if ($sidebar.hasClass('active')) {
      sidebarSwitch = true;
   } else {
      sidebarSwitch = false;
   }
});
