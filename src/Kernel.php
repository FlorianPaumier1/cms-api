<?php

namespace App;

use Symfony\Bundle\FrameworkBundle\Kernel\MicroKernelTrait;
use Symfony\Component\DependencyInjection\Loader\Configurator\ContainerConfigurator;
use Symfony\Component\HttpKernel\Kernel as BaseKernel;
use Symfony\Component\Routing\Loader\Configurator\RoutingConfigurator;

// Some swagger documentation
define('DESCRIPTION_GET', 'Retrieves a resource.');
define('DESCRIPTION_GET_ALL', 'Retrieves a collection of resources.');
define('DESCRIPTION_POST', 'Creates a resource.');
define('DESCRIPTION_PUT', 'Changes the resource.');
define('DESCRIPTION_DELETE', 'Removes the resource.');

define('DESCRIPTION_RESPONSE_200', 'Operation successful');
define('DESCRIPTION_RESPONSE_201', 'Resource created successfully');
define('DESCRIPTION_RESPONSE_204', 'No content');
define('DESCRIPTION_RESPONSE_400', 'Bad request');
define('DESCRIPTION_RESPONSE_401', 'Unauthorized');
define('DESCRIPTION_RESPONSE_403', 'Access denied');
define('DESCRIPTION_RESPONSE_404', 'Resource not found');

class Kernel extends BaseKernel
{
    use MicroKernelTrait;

    protected function configureContainer(ContainerConfigurator $container): void
    {
        $container->import('../config/{packages}/*.yaml');
        $container->import('../config/{packages}/'.$this->environment.'/*.yaml');

        if (is_file(\dirname(__DIR__).'/config/services.yaml')) {
            $container->import('../config/{services}.yaml');
            $container->import('../config/{services}_'.$this->environment.'.yaml');
        } elseif (is_file($path = \dirname(__DIR__).'/config/services.php')) {
            (require $path)($container->withPath($path), $this);
        }
    }

    protected function configureRoutes(RoutingConfigurator $routes): void
    {
        $routes->import('../config/{routes}/'.$this->environment.'/*.yaml');
        $routes->import('../config/{routes}/*.yaml');

        if (is_file(\dirname(__DIR__).'/config/routes.yaml')) {
            $routes->import('../config/{routes}.yaml');
        } elseif (is_file($path = \dirname(__DIR__).'/config/routes.php')) {
            (require $path)($routes->withPath($path), $this);
        }
    }
}
