<?php

namespace App\Repository;

use App\Entity\Event;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use FOS\RestBundle\Request\ParamFetcher;

/**
 * @method Event|null find($id, $lockMode = null, $lockVersion = null)
 * @method Event|null findOneBy(array $criteria, array $orderBy = null)
 * @method Event[]    findAll()
 * @method Event[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class EventRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Event::class);
    }

    // /**
    //  * @return Event[] Returns an array of Event objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('e.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Event
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
    public function findEvents(array $criteria)
    {
        $start = !is_null($criteria["date_start"]) && $criteria["date_start"] !== "null" ? (new \DateTime($criteria["date_start"]))->format('Y-m-d') : (new \DateTime())->format("Y-m-1");
        $end = !is_null($criteria["date_end"]) && $criteria["date_end"] !== "null"  ? (new \DateTime($criteria["date_end"]))->format('Y-m-d') : (new \DateTime())->format("Y-m-t");

        $q = $this->createQueryBuilder("e")
            ->where("e.publishedAt > :now")
            ->andWhere('e.takePlaceAt < :end')
            ->andWhere('e.takePlaceAt > :start')
            ->setParameter("now", (new \DateTime())->format("Y-m-d"))
            ->setParameter('end', $end)
            ->setParameter('start', $start);


        return $q->getQuery()->getResult();
    }
}
