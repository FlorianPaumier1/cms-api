<?php


namespace App\Repository;

use App\Entity\Backend\Contact\ContactForm;
use App\Entity\Backend\Contact\ContactInput;
use App\Entity\Contact;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ContactForm|null find($id, $lockMode = null, $lockVersion = null)
 * @method ContactForm|null findOneBy(array $criteria, array $orderBy = null)
 * @method ContactForm[]    findAll()
 * @method ContactForm[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ContactFormRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ContactForm::class);
    }

    public function disableAll()
    {
        $this->createQueryBuilder("c")
            ->set("c.enabled", false)
            ->getQuery()
            ->getResult()
        ;
    }
}
