<?php

namespace App\Repository;

use App\Entity\User;
use DateTime;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Symfony\Component\Security\Core\User\PasswordUpgraderInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use function get_class;

/**
 * @method User|null find($id, $lockMode = null, $lockVersion = null)
 * @method User|null findOneBy(array $criteria, array $orderBy = null)
 * @method User[]    findAll()
 * @method User[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserRepository extends ServiceEntityRepository implements PasswordUpgraderInterface
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, User::class);
    }

    /**
     * Used to upgrade (rehash) the user's password automatically over time.
     */
    public function upgradePassword(UserInterface $user, string $newEncodedPassword): void
    {
        if (!$user instanceof User) {
            throw new UnsupportedUserException(sprintf('Instances of "%s" are not supported.', get_class($user)));
        }

        $user->setPassword($newEncodedPassword);
        $this->_em->persist($user);
        $this->_em->flush();
    }

    public function loginUser(array $data)
    {
        return $this->createQueryBuilder("u")
            ->where("u.username = :login")
            ->orWhere("u.email = :login")
            ->andWhere("u.isEnabled = true")
            ->setParameter("login", $data["login"])
            ->getQuery()
            ->getOneOrNullResult()
            ;
    }

    public function findStatByDate(DateTime $start = null, DateTime $end = null)
    {

        $q = $this->createQueryBuilder("u");

        $startRange = $start ?? (new DateTime("-3 months"))->format("Y-m-1");
        $endRange = $end ?? (new DateTime())->format("Y-m-d");

        $q->where('u.createdAt BETWEEN :start AND :end')
            ->setParameters(["start" => $startRange, "end" => $endRange]);

        return $q->getQuery()->getResult();
    }

    public function userExist(string $username, string $email)
    {
        return $this->createQueryBuilder("u")
                ->select("count(u.id)")
                ->where("u.username = :username")
                ->orWhere("u.email = :email")
                ->getQuery()
                ->getSingleScalarResult() > 1;
    }
}
