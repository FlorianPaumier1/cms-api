<?php

namespace App\Repository;

use App\Entity\LessonChapter;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method LessonChapter|null find($id, $lockMode = null, $lockVersion = null)
 * @method LessonChapter|null findOneBy(array $criteria, array $orderBy = null)
 * @method LessonChapter[]    findAll()
 * @method LessonChapter[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class LessonChapterRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, LessonChapter::class);
    }

    // /**
    //  * @return LessonChapter[] Returns an array of LessonChapter objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('l')
            ->andWhere('l.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('l.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?LessonChapter
    {
        return $this->createQueryBuilder('l')
            ->andWhere('l.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
