<?php

namespace App\Repository;

use App\Entity\LessonChapterBlock;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method LessonChapterBlock|null find($id, $lockMode = null, $lockVersion = null)
 * @method LessonChapterBlock|null findOneBy(array $criteria, array $orderBy = null)
 * @method LessonChapterBlock[]    findAll()
 * @method LessonChapterBlock[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class LessonChapterBlockRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, LessonChapterBlock::class);
    }

    // /**
    //  * @return LessonChapterBlock[] Returns an array of LessonChapterBlock objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('l')
            ->andWhere('l.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('l.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?LessonChapterBlock
    {
        return $this->createQueryBuilder('l')
            ->andWhere('l.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
