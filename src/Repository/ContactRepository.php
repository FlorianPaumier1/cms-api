<?php

namespace App\Repository;

use App\Entity\Contact;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Contact|null find($id, $lockMode = null, $lockVersion = null)
 * @method Contact|null findOneBy(array $criteria, array $orderBy = null)
 * @method Contact[]    findAll()
 * @method Contact[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ContactRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Contact::class);
    }

    public function findStatByDate(\DateTime $start = null, \DateTime $end = null){

        $q = $this->createQueryBuilder("c");

        $startRange = $start ?? (new \DateTime("-3 months"))->format("Y-m-1");
        $endRange = $end ?? (new \DateTime())->format("Y-m-d");

        $q->where('c.createdAt BETWEEN :start AND :end')
            ->setParameters(["start" => $startRange, "end" => $endRange])
        ;

        return $q->getQuery()->getResult();
    }
}
