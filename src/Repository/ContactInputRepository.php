<?php


namespace App\Repository;

use App\Entity\Backend\Contact\ContactInput;
use App\Entity\Contact;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ContactInput|null find($id, $lockMode = null, $lockVersion = null)
 * @method ContactInput|null findOneBy(array $criteria, array $orderBy = null)
 * @method ContactInput[]    findAll()
 * @method ContactInput[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ContactInputRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ContactInput::class);
    }
    
}
