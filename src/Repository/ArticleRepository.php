<?php

namespace App\Repository;

use App\Entity\Article;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Article|null find($id, $lockMode = null, $lockVersion = null)
 * @method Article|null findOneBy(array $criteria, array $orderBy = null)
 * @method Article[]    findAll()
 * @method Article[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ArticleRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Article::class);
    }

    public function findStatByDate(\DateTime $start = null, \DateTime $end = null){

        $q = $this->createQueryBuilder("u");

        $startRange = $start ?? (new \DateTime("-3 months"))->format("Y-m-1");
        $endRange = $end ?? (new \DateTime())->format("Y-m-d");

        $q->where('u.createdAt BETWEEN :start AND :end')
            ->setParameters(["start" => $startRange, "end" => $endRange])
        ;

        return $q->getQuery()->getResult();
    }
}
