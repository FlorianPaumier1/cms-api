<?php

namespace App\Repository;

use App\Entity\LessonHistory;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method LessonHistory|null find($id, $lockMode = null, $lockVersion = null)
 * @method LessonHistory|null findOneBy(array $criteria, array $orderBy = null)
 * @method LessonHistory[]    findAll()
 * @method LessonHistory[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class LessonHistoryRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, LessonHistory::class);
    }

    // /**
    //  * @return LessonHistory[] Returns an array of LessonHistory objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('l')
            ->andWhere('l.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('l.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?LessonHistory
    {
        return $this->createQueryBuilder('l')
            ->andWhere('l.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
