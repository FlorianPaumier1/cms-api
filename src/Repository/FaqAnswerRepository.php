<?php

namespace App\Repository;

use App\Entity\FaqAnswer;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method FaqAnswer|null find($id, $lockMode = null, $lockVersion = null)
 * @method FaqAnswer|null findOneBy(array $criteria, array $orderBy = null)
 * @method FaqAnswer[]    findAll()
 * @method FaqAnswer[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class FaqAnswerRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, FaqAnswer::class);
    }

    // /**
    //  * @return FaqAnswer[] Returns an array of FaqAnswer objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('f.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?FaqAnswer
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
