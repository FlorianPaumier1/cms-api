<?php

namespace App\Repository\Backend;

use App\Entity\Backend\EvaluationUser;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method EvaluationUser|null find($id, $lockMode = null, $lockVersion = null)
 * @method EvaluationUser|null findOneBy(array $criteria, array $orderBy = null)
 * @method EvaluationUser[]    findAll()
 * @method EvaluationUser[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class EvaluationUserRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, EvaluationUser::class);
    }

    // /**
    //  * @return EvaluationUser[] Returns an array of EvaluationUser objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('e.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?EvaluationUser
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
