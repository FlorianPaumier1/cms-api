<?php

namespace App\Repository\Backend;

use App\Entity\Backend\EvaluationAnswer;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method EvaluationAnswer|null find($id, $lockMode = null, $lockVersion = null)
 * @method EvaluationAnswer|null findOneBy(array $criteria, array $orderBy = null)
 * @method EvaluationAnswer[]    findAll()
 * @method EvaluationAnswer[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class EvaluationAnswerRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, EvaluationAnswer::class);
    }

    // /**
    //  * @return EvaluationAnswer[] Returns an array of EvaluationAnswer objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('e.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?EvaluationAnswer
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
