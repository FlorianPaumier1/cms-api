<?php

namespace App\Repository\Backend;

use App\Entity\Backend\EvaluationQuestion;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method EvaluationQuestion|null find($id, $lockMode = null, $lockVersion = null)
 * @method EvaluationQuestion|null findOneBy(array $criteria, array $orderBy = null)
 * @method EvaluationQuestion[]    findAll()
 * @method EvaluationQuestion[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class EvaluationQuestionRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, EvaluationQuestion::class);
    }

    // /**
    //  * @return EvaluationQuestion[] Returns an array of EvaluationQuestion objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('e.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?EvaluationQuestion
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
