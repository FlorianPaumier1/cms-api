<?php

namespace App\Repository;

use App\Entity\SondageQuery;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method SondageQuery|null find($id, $lockMode = null, $lockVersion = null)
 * @method SondageQuery|null findOneBy(array $criteria, array $orderBy = null)
 * @method SondageQuery[]    findAll()
 * @method SondageQuery[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SondageQueryRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, SondageQuery::class);
    }

    // /**
    //  * @return SondageQuery[] Returns an array of SondageQuery objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?SondageQuery
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
