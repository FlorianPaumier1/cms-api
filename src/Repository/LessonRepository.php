<?php

namespace App\Repository;

use App\Entity\Lesson;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Lesson|null find($id, $lockMode = null, $lockVersion = null)
 * @method Lesson|null findOneBy(array $criteria, array $orderBy = null)
 * @method Lesson[]    findAll()
 * @method Lesson[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class LessonRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Lesson::class);
    }

    public function findStatByDate(\DateTime $start = null, \DateTime $end = null){

        $q = $this->createQueryBuilder("u");

        $startRange = $start ?? (new \DateTime("-3 months"))->format("Y-m-1");
        $endRange = $end ?? (new \DateTime())->format("Y-m-d");

        $q->where('u.createdAt BETWEEN :start AND :end')
            ->setParameters(["start" => $startRange, "end" => $endRange])
        ;

        return $q->getQuery()->getResult();
    }
}
