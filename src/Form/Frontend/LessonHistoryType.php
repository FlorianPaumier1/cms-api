<?php

namespace App\Form\Frontend;

use App\Entity\Lesson;
use App\Entity\LessonChapter;
use App\Entity\LessonHistory;
use phpDocumentor\Reflection\Types\Integer;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class LessonHistoryType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('chapter', EntityType::class, [
                "class" => LessonChapter::class,
                "required" => true,
            ])
            ->add('isOver', IntegerType::class, [
                "required" => true,
            ])
            ->add('lesson', EntityType::class, [
                "required" => true,
                "class" => Lesson::class,
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => LessonHistory::class,
        ]);
    }
}
