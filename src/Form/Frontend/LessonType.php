<?php

namespace App\Form\Frontend;

use App\Entity\Category;
use App\Entity\Lesson;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Vich\UploaderBundle\Form\Type\VichImageType;

class LessonType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', TextType::class, [
                "required" => true,
            ])
            ->add('description', TextareaType::class, [
                "required" => true,
            ])
            ->add('thumbnail', VichImageType::class)
            ->add('categories', EntityType::class, [
                "required" => true,
                "class" => Category::class,
            ])
            ->add("lessonChapters", CollectionType::class, [
                "required" => true,
                "allow_add" => true,
                "allow_delete" => true,
                "by_reference" => true,
                "data_class" => LessonChapterType::class
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Lesson::class,
        ]);
    }
}
