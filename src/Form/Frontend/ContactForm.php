<?php

namespace App\Form\Frontend;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TelType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\DateType;


class ContactForm extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $option): FormBuilderInterface
    {
        return $builder
            ->add('input1', TextType::class, array(
                'label' => 'Input 1',
                'row_attr' =>
                    array(
                        'placeholder' => 'input text',
                        'id' => 'input1',
                    ),
                'required' => true,
            ))
            ->add('input2', TelType::class, array(
                'label' => 'Input 2',
                'row_attr' =>
                    array(
                        'placeholder' => 'input phone',
                        'id' => 'input2',
                    ),
                'required' => true,
            ))
            ->add('input3', CheckboxType::class, array(
                'label' => 'Input 3',
                'row_attr' =>
                    array(
                        'placeholder' => 'input checkox',
                        'id' => 'input3',
                    ),
                'required' => true,
            ))
            ->add('input4', SubmitType::class, array(
                'label' => 'Input 4',
                'row_attr' =>
                    array(
                        'placeholder' => 'input submit',
                        'id' => 'input4',
                    ),
            ))
            ->add('input5', TextType::class, array(
                'label' => 'Input 5',
                'row_attr' =>
                    array(
                        'placeholder' => 'input date',
                        'id' => 'input5',
                    ),
                'required' => true,
            ));
    }

}
