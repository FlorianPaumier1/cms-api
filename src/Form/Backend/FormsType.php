<?php


namespace App\Form\Backend;


use App\Form\Frontend\ArticleType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

class FormsType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        return $builder
            ->add("name", TextType::class, [
                "label" => "Nom :",
                "required" => true
            ])
            ->add("enabled", CheckboxType::class, [
                "label" => "Activer ?",
                "required" => true
            ])
            ->add("fields", CollectionType::class, [
                "entry_type" => FormInputType::class,
                "allow_add" => true,
                "allow_delete" => true,
                "entry_options" => [
                    "label" => false,
                ],
                "required" => true,
            ])
            ;
    }
}
