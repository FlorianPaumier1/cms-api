<?php

namespace App\Form\Backend;

use App\Entity\Lesson;
use App\Entity\LessonChapter;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class LessonChapterType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', TextType::class, [
                "required" => true,
            ])
            ->add('position', IntegerType::class, [
                "required" => true,
            ])
            ->add("lessonChapterBlocks", CollectionType::class, [
                "required" => true,
                "allow_add" => true,
                "allow_delete" => true,
                "by_reference" => true,
                "data_class" => LessonChapterBlockType::class
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => LessonChapter::class,
        ]);
    }
}
