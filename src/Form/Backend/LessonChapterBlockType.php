<?php

namespace App\Form\Backend;

use App\Entity\LessonChapter;
use App\Entity\LessonChapterBlock;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class LessonChapterBlockType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', TextType::class, [
                "required" => true,
            ])
            ->add('content', TextareaType::class, [
                "required" => true,
            ])
            ->add('position', IntegerType::class, [
                "required" => true,
            ])
            ->add('LessonChapter', EntityType::class, [
                "required" => true,
                "class" => LessonChapter::class,
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => LessonChapterBlock::class,
        ]);
    }
}
