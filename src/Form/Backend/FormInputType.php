<?php


namespace App\Form\Backend;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

class FormInputType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        return $builder
            ->add("label", TextType::class)
            ->add("placeholder", TextType::class)
            ->add("type", ChoiceType::class, [
                "placeholder" => "Choisissez le type de l'élement",
                "choices" => [
                    "Text" => "text",
                    "Checkbox" => "checkbox",
                    "Nombre" => "integer",
                    "Date" => "date",
                    "Téléphone" => "phone",
                    "Adresse" => "address",
                    "TextArea" => "textarea",
                    "Email" => "email",
                    "Bouton" => "submit",
                ]
            ])
            ->add("required", CheckboxType::class, [
                "empty_data" => false
            ])
            ->add("id", HiddenType::class, [
                "required" => false,
            ])
            ->add("positionElement", HiddenType::class, [
                "required" => true,
            ])
            ->add("choices", TextareaType::class, [
                "required" => false
            ])
            ;
    }
}
