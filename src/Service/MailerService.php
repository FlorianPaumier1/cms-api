<?php


namespace App\Service;


use App\Entity\Backend\Contact\ContactObject;
use App\Entity\Contact;
use App\Entity\User;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\Mailer\Exception\TransportException;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;
use Symfony\Component\Mime\RawMessage;

class MailerService
{

    private $form = "nopreply@cms.fr";

    /** @var MailerInterface */
    private $mailer;

    public function __construct(MailerInterface $mailer)
    {
        $this->mailer = $mailer;
    }

    public function confirmationUser(User $user)
    {
        $message = (new TemplatedEmail())
            ->from($this->form)
            ->to($user->getEmail())
            ->subject('Confirmation de votre email')
            ->htmlTemplate("mailer/confirmation_user.html.twig")
            ->context([
                "user" => $user,
            ])
        ;

        return $this->send($message);
    }

    public function sendContactToAdmin(ContactObject $contact)
    {
        $message = (new TemplatedEmail())
            ->to($this->form)
            ->from("admin@noreply.com")
            ->subject('Demande de contact')
            ->htmlTemplate("mailer/contact.html.twig")
        ;

        return $this->send($message);
    }

    private function send(RawMessage $message){
        try {
            $this->mailer->send($message);
        }catch (TransportExceptionInterface $e){
            return [
                "error" => true,
                "message" => $e->getMessage()
            ];
        }

        return ["error" => false];
    }

    public function forgotPassword(User $user)
    {
        $message = (new TemplatedEmail())
            ->from($this->form)
            ->to($user->getEmail())
            ->subject('Modification de votre mot de passe')
            ->htmlTemplate("mailer/forgot_password.html.twig")
            ->context([
                "user" => $user,
            ])
        ;

        return $this->send($message);
    }
}
