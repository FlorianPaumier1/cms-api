<?php


namespace App\Service;


class CmsStatisticsService
{


    public function formatToCharts(array $items, string $type = ""): array
    {
        $data = [
            "label" => $type,
            "data" => []
        ];

        foreach ($items as $item){
            $month = $item->getCreatedAt()->format("Y-m");

            if(key_exists($month, $data["data"])) {
                $data["data"][$month] += 1;
            }else{
                $data["data"][$month] = 1;
            }
        }

        return $data;
    }

}
