<?php


namespace App\Service\Backend\Form;


use Doctrine\Common\Collections\Collection;

interface GenerateFormInterface
{

    public function getFields();
    public function addField(GenerateFormInputInterface $input);
    public function setFields(array $fields = null);
}
