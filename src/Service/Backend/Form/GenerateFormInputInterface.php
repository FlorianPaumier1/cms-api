<?php


namespace App\Service\Backend\Form;

interface GenerateFormInputInterface
{

    /**
     * @return string
     */
     function getSlug(): ?string;

    /**
     * @param string $formKey
     * @return GenerateFormInputInterface
     */
     function setSlug(string $formKey): self;

    /**
     * @return string
     */
     function getLabel(): ?string;

    /**
     * @param string $label
     * @return GenerateFormInputInterface
     */
     function setLabel(string $label): self;

    /**
     * @return string
     */
     function getType(): ?string;

    /**
     * @param string $type
     * @return self;
     */
     function setType(string $type): self;


    /**
     * @return bool
     */
     function isRequired(): ?bool;

    /**
     * @param bool $required
     * @return self;
     */
     function setRequired(bool $required): self;

    /**
     * @return string
     */
     function getPlaceholder(): ?string;

    /**
     * @param string $placeholder
     * @return self;
     */
     function setPlaceholder(string $placeholder): self;

    /**
     * @return string
     */
     function getChoices(): ?string;

    /**
     * @param string $choices
     * @return self;
     */
     function setChoices(?string $choices): self;

    /**
     * @return int
     */
     function getPositionElement(): int;

    /**
     * @param int $position
     * @return self;
     */
     function setPositionElement(int $position): self;

}
