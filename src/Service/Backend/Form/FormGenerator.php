<?php


namespace App\Service\Backend\Form;

use App\Entity\Backend\Contact\ContactForm;
use App\Entity\Backend\Contact\ContactInput;
use App\Entity\Backend\Evaluation\Evaluation;
use App\Entity\Backend\Evaluation\EvaluationInput;
use App\Repository\ContactFormRepository;
use App\Service\Backend\Form\GenerateFormInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bridge\Twig\Form\TwigRendererEngine;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TelType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\String\UnicodeString;
use Twig\Environment;
use function Symfony\Component\String\u;

class FormGenerator
{
    const AuthorizeEntryName = [
        "label_input",
        "type_input",
        "require_input",
        "placeholder_input",
        "position_input"
    ];

    private array $data = [];
    private array $fields = [];
    private array $usesType = [];
    private Environment $twig;
    private array $inputs = [];

    /** @var Serializer */
    private $serializer;

    private GenerateFormInterface $form;

    private EntityManagerInterface $entityManager;

    public function __construct(SerializerInterface $serializer, Environment $twig, EntityManagerInterface $entityManager)
    {
        $this->serializer = $serializer;
        $this->twig = $twig;
        $this->entityManager = $entityManager;
    }

    public function setData(array $data): self
    {
        $this->data = $data;
        return $this;
    }

    public function generateForm(GenerateFormInterface &$form): self
    {
        $this->form = $form;
        $this->form->setFields();

        if ($form instanceof ContactForm) {
            $fieldClass = ContactInput::class;
        }elseif ($form instanceof Evaluation){
            $fieldClass = EvaluationInput::class;
        }else{
            throw new \Exception("the class ". get_class($form) . "is not handle");
        }

        uasort($this->data["fields"], function ($a, $b) {
            return $a["positionElement"] > $b["positionElement"];
        });

        foreach ($this->data["fields"] as $field) {
            $slug = u($field["label"])->snake()->toString();
            $this->fields[$slug] = [
                "parameters" => [
                    "label" => $field["label"],
                    "row_attr" => [
                        "placeholder" => $field["placeholder"],
                        "id" => $slug,
                    ]
                ],
                "type" => $this->translateType($field["type"]),
            ];

            if($field["type"] !== "submit"){
                $this->fields[$slug]["parameters"] = array_merge($this->fields[$slug]["parameters"], ["required" => $field["required"]]);
            }
            if(!isset($field["choices"]) && !empty($field["choices"])){
                $this->fields[$slug]["parameters"] = array_merge($this->fields[$slug]["parameters"], ["choices" => $this->translateChoices($field["choices"])]);

            }

            $field = $field["id"] ? $this->entityManager->getRepository($fieldClass)->find($field["id"]) : new $fieldClass();

            $this->form->addField($field
                ->setSlug($slug)
                ->setType($field["type"])
                ->setPlaceholder($field["placeholder"])
                ->setLabel($field["label"])
                ->setRequired(!is_null($field["required"]))
                ->setPositionElement($field["positionElement"])
                ->setChoices(isset($this->fields[$slug]["parameters"]["choices"]) ? $this->fields[$slug]["parameters"]["choices"] : ""));
        }


        return $this;
    }

    public function createFormTypeClass()
    {

        $className = $this->data["name"];

        $this->generateFile([
            "form_fields" => $this->fields,
            "usesType" => array_unique($this->usesType),
            "class_name" => u($className)->camel()->title()->toString() . "Form",
            "prefix" => u($className)->snake()
        ]);
    }

    public function getInputs()
    {
        return $this->inputs;
    }

    private function translateType(string $type): string
    {

        $typeString = "";

        switch ($type) {
            case "text":
                $this->usesType[] = TextType::class;
                $typeString = "TextType::class";
                break;
            case "checkbox":
                $this->usesType[] = CheckboxType::class;
                $typeString = "CheckboxType::class";
                break;
            case "integer":
                $this->usesType[] = IntegerType::class;
                $typeString = "IntegerType::class";
                break;
            case "date":
                $this->usesType[] = DateType::class;
                $typeString = "TextType::class";
                break;
            case "phone":
                $this->usesType[] = TelType::class;
                $typeString = "TelType::class";
                break;
            case "address":
                $this->usesType[] = TextType::class;
                $typeString = "AddressType::class";
                break;
//            case "captcha":
//                $this->usesType[] = CaptchaType::class;
//                $typeString =  "CaptchaType::class";
//                break;
            case "textarea":
                $this->usesType[] = TextAreaType::class;
                $typeString = "TextAreaType::class";
                break;
            case "email":
                $this->usesType[] = EmailType::class;
                $typeString = "EmailType::class";
                break;
            case "submit":
                $this->usesType[] = SubmitType::class;
                $typeString = "SubmitType::class";
                break;
            default:
                $typeString = "TextType::class";
        }

        return $typeString;
    }

    private function generateFile(array $data)
    {
        ob_start();

        extract($data);
        require dirname(__DIR__, 4) . "/templates/backend/form/FormType.tpl.php";
        $content = ob_get_clean();
        $file = dirname(__DIR__, 3) . "/Form/Frontend/" . $data["class_name"] . ".php";
        file_put_contents($file, $content);

        $this->form->setPath($file);
    }

    private function translateChoices(string $choicesString = ""): string
    {
        if (empty($choicesString)) {
            return "";
        }

        $choices = [];

        $choicesArray = explode(",", $choicesString);
        foreach ($choicesArray as $choice) {
            $value = explode(":", $choice, 2);
            $choices[$value[0]] = $value[1];
        }

        return json_encode($choices);
    }
}
