<?php


namespace App\Service;


use Lcobucci\JWT\Builder;
use Lcobucci\JWT\Signer\Key;
use Lcobucci\JWT\Signer\Rsa\Sha256;

class MercureJwtProvider
{

    private string $secret;

    public function __construct()
    {
//        $this->secret = $secret;
    }

    public function __invoke(): string
    {
        return (new Builder)
            ->withClaim("mercure", ["publish" => ["*"]])
            ->getToken(new Sha256(), new Key("8DBpJa97GsQb"));
    }
}
