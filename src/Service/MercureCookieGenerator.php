<?php


namespace App\Service;


use App\Entity\User;
use Lcobucci\JWT\Builder;
use Lcobucci\JWT\Signer\Key;
use Lcobucci\JWT\Signer\Rsa\Sha256;
use Symfony\Component\Security\Core\Security;

class MercureCookieGenerator
{
    private string $url = "https://localhost:1337/hub";

    private array $access;
    private Security $security;
    /**
     * @var string
     */
    private string $secret;

    public function __construct(Security $security)
    {
        $this->access = [
            $this->url . "/hub/events",
            $this->url . "/hub/articles",
        ];

        $this->security = $security;

//        $this->secret = $secret;
    }

    public function generate(?User $user)
    {

        if ($user) {
            if (in_array("ROLE_USER", $user->getRoles())) {
                foreach ($user->getChatRooms() as $chatRoom) {
                    $this->access[] = `${$this->url}/chat/${$chatRoom->getId()}`;
                }

                $this->access[] = $this->url . "/lesson/new";
            }

            if (in_array("ROLE_ADMIN", $user->getRoles())) {
                $this->access[] = $this->url . "/contact";
            }
        }

        $token = (new Builder())
            ->withClaim("mercure", $this->access)
            ->getToken(new Sha256(), new Key("8DBpJa97GsQb"));

        return "mercureAuthorization=${token}; Path=/hub; HttpOnly;";
    }
}
