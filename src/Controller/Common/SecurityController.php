<?php


namespace App\Controller\Common;

use App\Controller\ApiController;
use App\Entity\User;
use App\Form\Common\UserLoginType;
use App\Form\Common\UserResetPasswordType;
use App\Service\MailerService;
use App\Service\Securizer;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Request\ParamFetcher;
use JMS\Serializer\SerializerInterface;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface;
use OpenApi\Annotations as OA;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Translation\Exception\NotFoundResourceException;

/**
 * @Rest\Route("/auth")
 * Class SecurityController
 * @package App\Controller\Common
 */
class SecurityController extends ApiController
{

    /**
     * @Rest\Get("/forgot-password")
     * @Rest\RequestParam(name="email", nullable=false)
     * @OA\Get(
     *     path="/forgot-password",
     *     tags={"City"},
     *     summary=DESCRIPTION_GET_ALL,
     *     @OA\Parameter(in="query", name="sort", description="field on which the sort is done", @OA\Schema(type="string", enum={"name","id"})),
     *     @OA\Parameter(in="query", name="direction", description="direction of the sort", @OA\Schema(type="string", enum={"asc","desc"})),
     *     @OA\Parameter(in="query", name="page", description="the page to return", @OA\Schema(type="integer")),
     *     @OA\Parameter(in="query", name="limit", description="the number of result per page", @OA\Schema(type="integer")),
     *     @OA\Response(response="200", description=DESCRIPTION_RESPONSE_200)
     * )
     * @param ParamFetcher $fetcher
     * @return JsonResponse
     */
    public function forgotPassword(ParamFetcher $fetcher)
    {
        /** @var User $user */
        $user = $this->getDoctrine()->getRepository(User::class)->findOneBy(["email" => $fetcher->get("email")]);

        if (!$user) {
            throw new NotFoundResourceException();
        }

        $token = $this->generateToken();

        $user->setForgetPasswordToken($token);
        $this->getDoctrine()->getManager()->persist($user);
        $this->getDoctrine()->getManager()->flush();

        /** @var MailerService $mailer */
        $mailer = $this->container->get("app.mailer");
        $mail = $mailer->forgotPassword($user);

        if ($mail["error"]) {
            return (new JsonResponse($mail["message"]))->setStatusCode(500);
        }

        return new JsonResponse("Un mail vous à été envoyé", 201);
    }

    /**
     * @Rest\Post("/reset-password")
     * @Rest\RequestParam(name="token", nullable=false)
     * @OA\Post(
     *     path="/reset-password",
     *     tags={"City"},
     *     summary=DESCRIPTION_POST,
     *     @OA\RequestBody(required=true),
     *     @OA\Response(response="201", description=DESCRIPTION_RESPONSE_201),
     *     @OA\Response(response="400", description=DESCRIPTION_RESPONSE_400),
     *     @OA\Response(response="401", description=DESCRIPTION_RESPONSE_401),
     *     @OA\Response(response="403", description=DESCRIPTION_RESPONSE_403)
     * )
     * @param Request $request
     * @param ParamFetcher $fetcher
     * @param SerializerInterface $serializer
     * @return JsonResponse
     */
    public function resetPassword(Request $request, ParamFetcher $fetcher, SerializerInterface $serializer)
    {
        /** @var User $user */
        $user = $this->getDoctrine()->getRepository(User::class)->findOneBy(["forgetPasswordToken" => $fetcher->get("token")]);

        if (!$user) {
            throw new NotFoundResourceException();
        }

        $form = $this->createForm(UserResetPasswordType::class, $user);
        $this->submit($form, $request);

        if ($form->isValid()) {
            $this->getDoctrine()->getManager()->persist($user);
            $this->getDoctrine()->getManager()->flush();

            return new JsonResponse("Mot de passe correctement modifié", 200);
        }

        return new JsonResponse($serializer->serialize(["errors" => $form->getErrors()], 'json'), 500);
    }

    /**
     * @OA\Post(
     *     path="/auth/login",
     *     @OA\Response(response="200", description="Successful operation"),
     *     @OA\Response(response="500", description="Error"),
     * )
     * @Rest\Route("/login", name="app_frontend_login", methods={"POST"})
     * @Rest\QueryParam(name="admin", default="false")
     * @Rest\View(serializerGroups={"user_login", "user_light"})
     * @param Request $request
     * @param SerializerInterface $serializer
     * @param JWTTokenManagerInterface $manager
     * @param ParamFetcher $fetcher
     * @param Securizer $securizer
     * @return JsonResponse
     */
    public function login(Request $request, SerializerInterface $serializer, JWTTokenManagerInterface $manager, ParamFetcher $fetcher, Securizer $securizer)
    {
        $form = $this->createForm(UserLoginType::class);
        $this->submit($form, $request);

        if ($form->isValid()) {
            $data = $form->getData();
            /** @var User $user */
            $user = $this->getDoctrine()->getRepository(User::class)->loginUser($data);

            if ((!$user || !password_verify($data["password"], $user->getPassword()))
                || ($fetcher->get("admin") === true && !$securizer->isGranted($user, "ROLE_ADMIN"))) {
                return new JsonResponse(["error" => "Votre Login ou Mot de passe n'est pas valide"], 404);
            }
            $user->setPlainPassword($user->getPassword());
            return new JsonResponse(["token" => $manager->create($user), "user" => $serializer->serialize($user, 'json')]);
        }

        return new JsonResponse($serializer->serialize(["errors" => $form->getErrors()], 'json'), 500);
    }
}
