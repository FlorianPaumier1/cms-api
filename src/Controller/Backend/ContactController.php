<?php

namespace App\Controller\Backend;

use App\Entity\Backend\Contact\ContactForm;
use App\Entity\Backend\Contact\ContactInput;
use App\Form\Backend\FormsType;
use App\Model\Representation\Pagination;
use App\Repository\ContactInputRepository;
use App\Repository\ContactRepository;
use App\Service\Backend\Form\FormGenerator;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Request\ParamFetcher;
use JMS\Serializer\SerializerInterface;
use Knp\Component\Pager\Pagination\PaginationInterface;
use Knp\Component\Pager\PaginatorInterface;
use OpenApi\Annotations as OA;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Constraints\Json;

/**
 * @Rest\Route("/contact")
 * Class ContactController
 * @package App\Controller\Backend
 */
class ContactController extends AbstractController
{

    /**
     * @Rest\Get(name="admin_contact_list")
     * @Rest\View(serializerGroups={"contact_light", "pagination"})
     * @Rest\QueryParam(name="sort", default="id", requirements="name|id")
     * @Rest\QueryParam(name="direction", default="asc", requirements="asc|desc")
     * @Rest\QueryParam(name="page", default=1, requirements="\d+")
     * @Rest\QueryParam(name="limit", default=Pagination::DEFAULT_LIMIT, requirements="\d+")
     * @OA\Get(
     *     path="/admin/contact",
     *     tags={"Admin/Contact"},
     *     summary=DESCRIPTION_GET_ALL,
     *     @OA\Parameter(in="query", name="sort", description="field on which the sort is done", @OA\Schema(type="string", enum={"name","id"})),
     *     @OA\Parameter(in="query", name="direction", description="direction of the sort", @OA\Schema(type="string", enum={"asc","desc"})),
     *     @OA\Parameter(in="query", name="page", description="the page to return", @OA\Schema(type="integer")),
     *     @OA\Parameter(in="query", name="limit", description="the number of result per page", @OA\Schema(type="integer")),
     *     @OA\Response(response="200", description=DESCRIPTION_RESPONSE_200)
     * )
     * @param ContactRepository $contactRepository
     * @param PaginatorInterface $paginator
     * @param ParamFetcher $fetcher
     * @return Pagination
     */
    public function index(ContactRepository $contactRepository, PaginatorInterface $paginator, ParamFetcher $fetcher): Pagination
    {
        return Pagination::paginate($contactRepository->createQueryBuilder("c"), $paginator, $fetcher);
    }

    /**
     * @Rest\Route(path="/new", name="admin_contact_new", methods={"GET","POST"})
     * @param Request $request
     * @param FormGenerator $formGenerator
     * @param SerializerInterface $serializer
     * @return array|JsonResponse
     */
    public function new(Request $request, FormGenerator $formGenerator, SerializerInterface $serializer)
    {

        $contactForm = new ContactForm();

        $form = $this->createForm(FormsType::class, $contactForm);

        $form->submit($request->request->all(), false);

        if ($request->getMethod() === "POST" && $form->isValid()) {

            $this->persitForm($formGenerator, $request, $contactForm);

            return new JsonResponse("Votre formualaire à bien été enregistrer", 201);
        }

        if ($form->getErrors()->count() > 0) {
            return new JsonResponse($serializer->serialize($form->getErrors(), "json"), 500);
        }

        return ["view" => $this->renderView("backend/contact/form.html.twig", ["form" => $form->createView()])];
    }

    /**
     * @Rest\Route("/{id}/edit", name="admin_edit_contact", methods={"GET", "PATCH"})
     * @param Request $request
     * @param SerializerInterface $serializer
     * @param ContactForm $contactForm
     * @param ContactInputRepository $contactInputRepository
     * @param FormGenerator $formGenerator
     * @return array|JsonResponse
     *
     */
    public function edit(Request $request, SerializerInterface $serializer, ContactForm $contactForm, FormGenerator $formGenerator)
    {
        $form = $this->createForm(FormsType::class, $contactForm);

        $form->submit($request->request->all(), false);

        if ($request->getMethod() === "PATCH" && $form->isValid()) {

            $this->persitForm($formGenerator, $request, $contactForm);

            return new JsonResponse("Votre formualaire à bien été enregistrer", 201);
        }

        if ($form->getErrors()->count() > 0) {
            return new JsonResponse($serializer->serialize($form->getErrors(), "json"), 500);
        }

        return ["view" => $this->renderView("backend/contact/form.html.twig", ["form" => $form->createView()])];
    }

    /**
     * @Rest\Get("/{id}")
     * @param ContactForm $contactForm
     */
    public function show(ContactForm $contactForm)
    {
        return $contactForm;
    }

    public function delete(ContactForm $contactForm)
    {
        $this->getDoctrine()->getManager()->remove($contactForm);
        return new JsonResponse("Bien supprimé", 204);
    }

    private function persitForm(
        FormGenerator $formGenerator,
        Request $request,
        ContactForm $contactForm
    ) {
        $formTypeBuilder = $formGenerator->setData($request->request->all())->generateForm($contactForm);
        $formTypeBuilder->createFormTypeClass();

        $em = $this->getDoctrine()->getManager();

        if ($contactForm->isEnabled()) {
            $this->getDoctrine()->getRepository(ContactForm::class)->disableAll();
        }

        $em->persist($contactForm);
        $em->flush();
    }
}
