<?php

namespace App\Controller\Backend;

use App\Controller\ApiController;
use App\Entity\Lesson;
use App\Form\Backend\LessonType;
use App\Model\Representation\Pagination;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Request\ParamFetcher;
use Knp\Component\Pager\PaginatorInterface;
use OpenApi\Annotations as OA;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

/**
 * @Rest\Route("/lesson")
 * Class LessonController
 * @package App\Controller\Backend
 */
class LessonController extends ApiController
{

    /**
     * @Security("is_granted('ROLE_ADMIN')")
     * @Rest\Get
     * @Rest\View(serializerGroups={"list", "pagination"})
     * @Rest\QueryParam(name="sort", default="id", requirements="name|id")
     * @Rest\QueryParam(name="direction", default="asc", requirements="asc|desc")
     * @Rest\QueryParam(name="page", default=1, requirements="\d+")
     * @Rest\QueryParam(name="limit", default=Pagination::DEFAULT_LIMIT, requirements="\d+")
     * @OA\Get(
     *     path="/admin/lesson",
     *     tags={"City"},
     *     summary=DESCRIPTION_GET_ALL,
     *     @OA\Parameter(in="query", name="sort", description="field on which the sort is done", @OA\Schema(type="string", enum={"name","id"})),
     *     @OA\Parameter(in="query", name="direction", description="direction of the sort", @OA\Schema(type="string", enum={"asc","desc"})),
     *     @OA\Parameter(in="query", name="page", description="the page to return", @OA\Schema(type="integer")),
     *     @OA\Parameter(in="query", name="limit", description="the number of result per page", @OA\Schema(type="integer")),
     *     @OA\Response(response="200", description=DESCRIPTION_RESPONSE_200)
     * )
     * @param ParamFetcher $fetcher
     * @param PaginatorInterface $paginator
     * @return Pagination
     */
    public function index(ParamFetcher $fetcher, PaginatorInterface $paginator)
    {
        return Pagination::paginate($this->getDoctrine()->getRepository(Lesson::class)->findAll(), $paginator, $fetcher);
    }

    /**
     * @Security("is_granted('ROLE_ADMIN')")
     * @Rest\Post
     * @Rest\View(serializerGroups={"city", "school_id", "country_id"})
     * @OA\Post(
     *     path="/admin/lesson",
     *     tags={"City"},
     *     summary=DESCRIPTION_POST,
     *     @OA\RequestBody(required=true),
     *     @OA\Response(response="201", description=DESCRIPTION_RESPONSE_201),
     *     @OA\Response(response="400", description=DESCRIPTION_RESPONSE_400),
     *     @OA\Response(response="401", description=DESCRIPTION_RESPONSE_401),
     *     @OA\Response(response="403", description=DESCRIPTION_RESPONSE_403)
     * )
     * @param Request $request
     * @param SerializerInterface $serializer
     * @return JsonResponse
     */
    public function new(Request $request, SerializerInterface $serializer)
    {
        $lesson = new Lesson();
        $form = $this->createForm(LessonType::class, $lesson);
        $this->submit($form, $request);

        if ($form->isValid()){
            $this->getDoctrine()->getManager()->persist($lesson);
            $this->getDoctrine()->getManager()->flush();

            return new JsonResponse("Votre cours a bien été créé", 201);
        }

        return new JsonResponse($serializer->serialize(["errors" => $form->getErrors()], 'json'), 500);
    }


    /**
     * @Security("is_granted('ROLE_ADMIN')")
     * @Rest\Get("/{id}")
     * @Rest\View(serializerGroups={"details"})
     * @OA\Get(
     *     path="/admin/lesson/{id}",
     *     tags={"City"},
     *     summary=DESCRIPTION_GET_ALL,
     *     @OA\Parameter(in="query", name="sort", description="field on which the sort is done", @OA\Schema(type="string", enum={"name","id"})),
     *     @OA\Parameter(in="query", name="direction", description="direction of the sort", @OA\Schema(type="string", enum={"asc","desc"})),
     *     @OA\Parameter(in="query", name="page", description="the page to return", @OA\Schema(type="integer")),
     *     @OA\Parameter(in="query", name="limit", description="the number of result per page", @OA\Schema(type="integer")),
     *     @OA\Response(response="200", description=DESCRIPTION_RESPONSE_200)
     * )
     * @param Lesson $lesson
     * @return Lesson
     */
    public function show(Lesson $lesson)
    {
        return $lesson;
    }

    /**
     * @Security("is_granted('ROLE_ADMIN')")
     * @Rest\Post("/{id}")
     * @OA\Post(
     *     path="/admin/lesson/{id}",
     *     tags={"City"},
     *     summary=DESCRIPTION_POST,
     *     @OA\RequestBody(required=true),
     *     @OA\Response(response="201", description=DESCRIPTION_RESPONSE_201),
     *     @OA\Response(response="400", description=DESCRIPTION_RESPONSE_400),
     *     @OA\Response(response="401", description=DESCRIPTION_RESPONSE_401),
     *     @OA\Response(response="403", description=DESCRIPTION_RESPONSE_403)
     * )
     * @param Request $request
     * @param Lesson $lesson
     * @param SerializerInterface $serializer
     * @return JsonResponse
     */
    public function edit(Request $request, Lesson $lesson, SerializerInterface $serializer)
    {
        $form = $this->createForm(LessonType::class, $lesson);
        $this->submit($form, $request);

        if ($form->isValid()){
            $this->getDoctrine()->getManager()->persist($lesson);
            $this->getDoctrine()->getManager()->flush();

            return new JsonResponse("Votre cours a bien été modifier", 200);
        }

        return new JsonResponse($serializer->serialize(["errors" => $form->getErrors()], 'json'), 500);
    }

    /**
     * @Rest\Delete("/{id}", requirements={"id"="\d+"})
     * @OA\Delete(
     *     path="/admin/lesson/{id}",
     *     tags={"JourneyApplication"},
     *     summary=DESCRIPTION_DELETE,
     *     @OA\Parameter(in="path", name="id", description="Ressource ID"),
     *     @OA\Response(response="204", description=DESCRIPTION_RESPONSE_204),
     *     @OA\Response(response="404", description=DESCRIPTION_RESPONSE_204),
     * )
     **/
    public function delete(Lesson $lesson)
    {
        $this->getDoctrine()->getManager()->remove($lesson);
        $this->getDoctrine()->getManager()->flush();

        return new JsonResponse("Votre cours a bien été supprimé", 200);
    }
}
