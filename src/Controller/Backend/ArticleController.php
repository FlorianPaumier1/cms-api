<?php

namespace App\Controller\Backend;

use App\Entity\Article;
use App\Form\Frontend\ArticleType;
use App\Model\Representation\Pagination;
use App\Repository\ArticleRepository;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Request\ParamFetcher;
use JMS\Serializer\SerializerInterface;
use Knp\Component\Pager\PaginatorInterface;
use OpenApi\Annotations as OA;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Rest\Route("/article")
 * Class ArticleController
 * @package App\Controller\Backend
 */
class ArticleController extends AbstractController
{
    /**
     * @Rest\Get
     * @Rest\View(serializerGroups={"article_light","user_light", "pagination"})
     * @Rest\QueryParam(name="sort", default="id", requirements="name|id")
     * @Rest\QueryParam(name="direction", default="asc", requirements="asc|desc")
     * @Rest\QueryParam(name="page", default=1, requirements="\d+")
     * @Rest\QueryParam(name="limit", default=Pagination::DEFAULT_LIMIT, requirements="\d+")
     * @OA\Get(
     *     path="/admin/article",
     *     tags={"Admin/Article"},
     *     summary=DESCRIPTION_GET_ALL,
     *     @OA\Parameter(in="query", name="sort", description="field on which the sort is done", @OA\Schema(type="string", enum={"name","id"})),
     *     @OA\Parameter(in="query", name="direction", description="direction of the sort", @OA\Schema(type="string", enum={"asc","desc"})),
     *     @OA\Parameter(in="query", name="page", description="the page to return", @OA\Schema(type="integer")),
     *     @OA\Parameter(in="query", name="limit", description="the number of result per page", @OA\Schema(type="integer")),
     *     @OA\Response(response="200", description=DESCRIPTION_RESPONSE_200)
     * )
     * @param ArticleRepository $articleRepository
     * @param PaginatorInterface $paginator
     * @param ParamFetcher $fetcher
     * @return Pagination
     */
    public function index(ArticleRepository $articleRepository, PaginatorInterface $paginator, ParamFetcher $fetcher): Pagination
    {
        return Pagination::paginate($articleRepository->createQueryBuilder("a"), $paginator, $fetcher);
    }

    /**
     * @Security("is_granted('ROLE_ADMIN')")
     * @Rest\Post
     * @OA\Post(
     *     path="/admin/article",
     *     tags={"Admin/Article"},
     *     summary=DESCRIPTION_POST,
     *     @OA\Response(response="201", description=DESCRIPTION_RESPONSE_201),
     *     @OA\Response(response="400", description=DESCRIPTION_RESPONSE_400),
     *     @OA\Response(response="401", description=DESCRIPTION_RESPONSE_401),
     *     @OA\Response(response="403", description=DESCRIPTION_RESPONSE_403)
     * )
     * @param Request $request
     * @param SerializerInterface $serializer
     * @return JsonResponse
     */
    public function new(Request $request, SerializerInterface $serializer): JsonResponse
    {
        $article = new Article();
        $form = $this->createForm(ArticleType::class, $article);
        $form->submit($request);

        if ($form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($article);
            $entityManager->flush();

            return (new JsonResponse("L'article à bien été créé"))->setStatusCode(201);
        }

        return new JsonResponse($serializer->serialize($form->getErrors(), "json"), 500);
    }

    /**
     * @Rest\Get(path="/{id}", requirements={"id"="\d+"})
     * @Rest\View(serializerGroups={"article"})
     * @OA\Get(
     *     path="/admin/article/{id}",
     *     tags={"Admin/Article"},
     *     summary=DESCRIPTION_GET,
     *     @OA\Response(response="200", description=DESCRIPTION_RESPONSE_200)
     * )
     * @param Article $article
     * @return Article
     */
    public function show(Article $article): Article
    {
        return $article;
    }

    /**
     * @Security("is_granted('ROLE_ADMIN')")
     * @Rest\Post("/{id}", requirements={"id"="\d+"})
     * @OA\Post(
     *     path="/admin/article/{id}",
     *     tags={"Admin/Article"},
     *     summary=DESCRIPTION_POST,
     *     @OA\Response(response="200", description=DESCRIPTION_RESPONSE_200),
     *     @OA\Response(response="400", description=DESCRIPTION_RESPONSE_400),
     *     @OA\Response(response="401", description=DESCRIPTION_RESPONSE_401),
     *     @OA\Response(response="403", description=DESCRIPTION_RESPONSE_403)
     * )
     * @param Request $request
     * @param Article $article
     * @param SerializerInterface $serializer
     * @return JsonResponse
     */
    public function edit(Request $request, Article $article, SerializerInterface $serializer): JsonResponse
    {
        $form = $this->createForm(ArticleType::class, $article);
        $form->submit($request);

        if ($form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return (new JsonResponse("L'article à bien été modifié"))->setStatusCode(200);
        }

        return new JsonResponse($serializer->serialize($form->getErrors(), "json"), 500);
    }

    /**
     * @Rest\Delete("/{id}", requirements={"id"="\d+"})
     * @OA\Delete(
     *     path="/admin/article/{id}",
     *     tags={"Admin/Article"},
     *     summary=DESCRIPTION_DELETE,
     *     @OA\Parameter(in="path", name="id", description="Ressource ID"),
     *     @OA\Response(response="204", description=DESCRIPTION_RESPONSE_204),
     *     @OA\Response(response="404", description=DESCRIPTION_RESPONSE_204),
     * )
     * @param Request $request
     * @param Article $article
     * @return JsonResponse
     */
    public function delete(Request $request, Article $article): JsonResponse
    {
        if ($this->isCsrfTokenValid('delete' . $article->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($article);
            $entityManager->flush();
        }

        return (new JsonResponse())->setStatusCode(204);
    }
}
