<?php


namespace App\Controller\Backend;


use App\Controller\ApiController;
use App\Entity\User;
use App\Model\Representation\Pagination;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Request\ParamFetcher;
use Knp\Component\Pager\Paginator;
use Knp\Component\Pager\PaginatorInterface;
use OpenApi\Annotations as OA;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Rest\Route("/users")
 * Class UserController
 * @package App\Controller\Backend
 */
class UserController extends ApiController
{

    /**
     * @Rest\Get
     * @Rest\View(serializerGroups={"list", "pagination"})
     * @Rest\QueryParam(name="sort", default="id", requirements="name|id")
     * @Rest\QueryParam(name="direction", default="asc", requirements="asc|desc")
     * @Rest\QueryParam(name="page", default=1, requirements="\d+")
     * @Rest\QueryParam(name="limit", default=Pagination::DEFAULT_LIMIT, requirements="\d+")
     * @OA\Get(
     *     path="/admin/user",
     *     tags={"City"},
     *     summary=DESCRIPTION_GET_ALL,
     *     @OA\Parameter(in="query", name="sort", description="field on which the sort is done", @OA\Schema(type="string", enum={"name","id"})),
     *     @OA\Parameter(in="query", name="direction", description="direction of the sort", @OA\Schema(type="string", enum={"asc","desc"})),
     *     @OA\Parameter(in="query", name="page", description="the page to return", @OA\Schema(type="integer")),
     *     @OA\Parameter(in="query", name="limit", description="the number of result per page", @OA\Schema(type="integer")),
     *     @OA\Response(response="200", description=DESCRIPTION_RESPONSE_200)
     * )
     * @param Request $request
     * @param ParamFetcher $fetcher
     * @param PaginatorInterface $paginator
     * @return Pagination
     */
    public function users(Request $request, ParamFetcher $fetcher, PaginatorInterface $paginator)
    {
        return Pagination::paginate($this->getDoctrine()->getRepository(User::class)->findAll(),$paginator, $fetcher);
    }
}
