<?php


namespace App\Controller\Backend;


use App\Controller\ApiController;
use App\Entity\User;
use App\Form\Common\UserLoginType;
use FOS\RestBundle\Controller\Annotations as Rest;
use JMS\Serializer\SerializerInterface;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface;
use OpenApi\Annotations as OA;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class SecurityController
 * @package App\Controller\Backend
 */
class SecurityController extends ApiController
{
    /**
     * @OA\Post(
     *     path="/admin/login",
     *     @OA\Response(response="200", description="Successful operation"),
     *     @OA\Response(response="500", description="Error"),
     * )
     * @Rest\Route("/login", name="app_admin_login", methods={"POST"})
     * @param Request $request
     */
    public function login(Request $request, SerializerInterface $serializer, JWTTokenManagerInterface $manager)
    {
        $form = $this->createForm(UserLoginType::class);
        $this->submit($form, $request);

        if ($form->isValid()) {
            $data = $form->getData();
            /** @var User $user */
            $user = $this->getDoctrine()->getRepository(User::class)->loginUser($data);

            if (!$user || !password_verify($data["password"], $user->getPassword())) {
                return new JsonResponse("Votre Login ou Mot de passe n'est pas valide", 500);
            }

            return new JsonResponse(["token" => $manager->create($user)]);
        }

        return new JsonResponse($serializer->serialize(["errors" => $form->getErrors()], 'json'), 500);
    }

}
