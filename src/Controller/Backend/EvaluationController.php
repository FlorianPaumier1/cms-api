<?php


namespace App\Controller\Backend;


use App\Controller\ApiController;
use App\Entity\Backend\Contact\ContactForm;
use App\Entity\Backend\Evaluation\Evaluation;
use App\Entity\Lesson;
use App\Form\Backend\FormsType;
use App\Service\Backend\Form\FormGenerator;
use FOS\RestBundle\Controller\Annotations as Rest;
use JMS\Serializer\SerializerInterface;
use Symfony\Component\Form\Extension\Core\Type\TimeType;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Rest\Route("/lesson/{lesson}/evaluation")
 * Class EvaluationController
 * @package App\Controller\Backend
 */
class EvaluationController extends ApiController
{

    /**
     * @Rest\Route("/", name="admin_lesson_evaluation_index", methods={"GET", "POST"})
     */
    public function index(Request $request, Lesson $lesson, FormGenerator $formGenerator, SerializerInterface $serializer)
    {
        $evaluation = $lesson->getEvaluation() ?? new Evaluation();

        $form = $this->createForm(FormsType::class, $evaluation);

        $form->submit($request->request->all(), false);

        if ($request->getMethod() === "POST" && $form->isValid()) {

            $this->persitForm($formGenerator, $request, $evaluation, $lesson);


            return new JsonResponse("Votre formualaire à bien été enregistrer", 201);
        }

        if ($form->getErrors()->count() > 0) {
            return new JsonResponse($serializer->serialize($form->getErrors(), "json"), 500);
        }

        return ["view" => $this->renderView("backend/contact/form.html.twig", ["form" => $form->createView()])];
    }

    private function persitForm(
        FormGenerator $formGenerator,
        Request $request,
        Evaluation $evaluation,
        Lesson $lesson
    ) {
        $formTypeBuilder = $formGenerator->setData($request->request->all())->generateForm($evaluation);
        $formTypeBuilder->createFormTypeClass();

        $lesson->setEvaluation($evaluation);
        $em = $this->getDoctrine()->getManager();

        $em->persist($lesson);
        $em->flush();
    }
}
