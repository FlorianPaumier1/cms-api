<?php


namespace App\Controller\Backend;


use App\Entity\Article;
use App\Entity\Contact;
use App\Entity\Lesson;
use App\Entity\User;
use App\Service\CmsStatisticsService;
use Doctrine\ORM\EntityManagerInterface;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Request\ParamFetcher;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * @Rest\Route("/")
 * Class DashboardController
 * @package App\Controller\Backend
 */
class DashboardController extends AbstractController
{

    /**
     * @Rest\Get("/statistics", name="app_backend_statistics")
     * @Rest\QueryParam(name="date_start", nullable=true)
     * @Rest\QueryParam(name="date_end", nullable=true)
     * @Rest\QueryParam(name="category", nullable=true)
     * @Rest\QueryParam(name="type", nullable=true)
     * @param ParamFetcher $fetcher
     * @param EntityManagerInterface $manager
     * @return array
     */
    public function statistics(ParamFetcher $fetcher, EntityManagerInterface $manager, CmsStatisticsService $cmsStatistics)
    {
        $users = $manager->getRepository(User::class)->findStatByDate();
        $lessons = $manager->getRepository(Lesson::class)->findStatByDate();
        $articles = $manager->getRepository(Article::class)->findStatByDate();
        $contacts = $manager->getRepository(Contact::class)->findStatByDate();

        return [
            $cmsStatistics->formatToCharts($users, "Users"),
            $cmsStatistics->formatToCharts($lessons, "Lessons"),
            $cmsStatistics->formatToCharts($articles, "Articles"),
            $cmsStatistics->formatToCharts($contacts, "Contacts"),
        ];
    }
}
