<?php

namespace App\Controller\Frontend;

use App\Entity\Event;
use App\Entity\EventInscription;
use App\Model\Representation\Pagination;
use App\Repository\EventRepository;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Request\ParamFetcher;
use Knp\Component\Pager\Paginator;
use Knp\Component\Pager\PaginatorInterface;
use OpenApi\Annotations as OA;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/event")
 */
class EventController extends AbstractController
{
    /**
     * @Rest\Get ("/", name="event_index")
     * @Rest\View(serializerGroups={"event_light", "pagination"})
     * @Rest\QueryParam(name="date_start", nullable=true)
     * @Rest\QueryParam(name="date_end", nullable=true)
     * @Rest\QueryParam(name="page", default="1")
     * @Rest\QueryParam(name="limit", default="20")
     * @param EventRepository $eventRepository
     * @param Paginator $paginator
     * @param ParamFetcher $fetcher
     * @return Pagination
     */
    public function index(EventRepository $eventRepository, PaginatorInterface $paginator, ParamFetcher $fetcher)
    {
        return Pagination::paginate($eventRepository->findEvents($fetcher->all()), $paginator, $fetcher);
    }

    /**
     * @Rest\Get("/{id}", name="event_show")
     * @Rest\View(serializerGroups={"event"})
     * @param Event $event
     * @return Event
     */
    public function show(Event $event): Event
    {
        return $event;
    }

    /**
     * @Rest\Post("/{id}/sign")
     * @OA\Post(
     *     path="/event/{id}/sign}",
     *     tags={"Event"},
     *     summary=DESCRIPTION_GET,
     *     @OA\Parameter(in="query", name="sort", description="field on which the sort is done"),
     *     @OA\Parameter(in="query", name="direction", description="direction of the sort"),
     *     @OA\Parameter(in="query", name="page", description="the page to return"),
     *     @OA\Parameter(in="query", name="limit", description="the number of result per page"),
     *     @OA\Response(response="200", description=DESCRIPTION_RESPONSE_200)
     * )
     */
    public function signEvent(Event $event)
    {
        $user = $this->getUser();

        if (!$user) {
            return new JsonResponse(["message" => "Vous devez être connecté pour pouvoir vous inscrire"], 401);
        }

        $sign = (new EventInscription())
            ->setUser($user)
            ->setEvent($event);

        $this->getDoctrine()->getManager()->persist($sign);
        $this->getDoctrine()->getManager()->flush();

        return new JsonResponse("Vous avez bien été inscrit. Au plaisir de vous voir !", 201);
    }
}
