<?php

namespace App\Controller\Frontend;

use App\Controller\ApiController;
use App\Entity\Backend\Contact\ContactInput;
use App\Entity\Backend\Contact\ContactMeta;
use App\Entity\Backend\Contact\ContactObject;
use App\Entity\Contact;
use App\Form\Frontend\PublicContactType;
use App\Repository\ContactFormRepository;
use App\Service\MailerService;
use FOS\RestBundle\Controller\Annotations as Rest;
use JMS\Serializer\SerializerInterface;
use OpenApi\Annotations as OA;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Rest\Route("/contact")
 */
class ContactController extends ApiController
{
    /**
     * @Rest\Get()
     * @param ContactFormRepository $repository
     * @return array
     */
    public function index(ContactFormRepository $repository)
    {
        $contactForm = $repository->findOneBy(["enabled" => true])->getPath();
        if(!class_exists($contactForm)){
            $contactForm = PublicContactType::class;
        }

        $form = $this->createForm($contactForm);
        return ["view" => $this->renderView("contact/_form.html.twig",["form" => $form->createView()]), "prefix" => $form->getName()];

    }

    /**
     * @OA\Post(
     *     path="/contact/new",
     *     tags={"Contact"},
     *     summary=DESCRIPTION_POST,
     *     @OA\RequestBody(required=true),
     *     @OA\Response(response="201", description=DESCRIPTION_RESPONSE_201),
     *     @OA\Response(response="400", description=DESCRIPTION_RESPONSE_400),
     *     @OA\Response(response="401", description=DESCRIPTION_RESPONSE_401),
     *     @OA\Response(response="403", description=DESCRIPTION_RESPONSE_403)
     * )
     * @Rest\Post("/new", name="frontend_contact_new")
     * @param Request $request
     * @param SerializerInterface $serializer
     * @param MailerService $mailer
     * @param ContactFormRepository $repository
     * @return JsonResponse
     */
    public function new(Request $request, SerializerInterface $serializer, MailerService $mailer,ContactFormRepository $repository)
    {
        $contactForm = $repository->findOneBy(["enabled" => true]);
        $formClass = $contactForm->getPath();
        if(!class_exists($formClass)){
            $formClass = PublicContactType::class;
        }

        $form = $this->createForm($formClass);
        $this->submit($form, $request);

        if ($form->isValid()) {

            $contact = (new ContactObject())
                ->setForm($contactForm);

            $entityManager = $this->getDoctrine()->getManager();

            foreach ($form->getData() as $key => $value){

                $input = $entityManager->getRepository(ContactInput::class)->findOneBy(["slug" => $key]);


                $contact->addInput(
                    (new ContactMeta())
                    ->setInput($input)
                    ->setContact($contact)
                    ->setValue($value)
                );
            }

            $entityManager->persist($contact);
            $entityManager->flush();

            $mail = $mailer->sendContactToAdmin($contact);

            if($mail["error"]){
                return new JsonResponse(["message" => $mail["message"]], 500);
            }

            return new JsonResponse(["message" => "Votre message a bien été envoyé"], 201);
        }

        return new JsonResponse($serializer->serialize($form->getErrors(), "json"), 500);
    }

    /**
     * @Rest\Get("/{id}", requirements={"id"="\d+"})
     * @Rest\View(serializerGroups={"detail"})
     * @OA\Get(
     *     path="/contact/{id}",
     *     tags={"Contact"},
     *     summary=DESCRIPTION_GET,
     *     @OA\Response(response="200", description=DESCRIPTION_RESPONSE_200)
     * )
     * @param Contact $contact
     * @return Contact
     */
    public function show(Contact $contact)
    {
        return $contact;
    }
}
