<?php

namespace App\Controller\Frontend;

use App\Entity\Category;
use App\Entity\Faq;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Rest\Route("/faq")
 * Class FaqController
 * @package App\Controller\Frontend
 */
class FaqController extends AbstractController
{
    /**
     * @Route("/", name="faq")
     * @Rest\View(serializerGroups={"faq_light", "category"})
     */
    public function index()
    {
        $faqs = $this->getDoctrine()->getRepository(Faq::class)->findAll();

        $array = [];

        foreach ($faqs as $faq){
            if(!array_key_exists($faq->getCategory()->getName(), $array) || count($array[$faq->getCategory()->getName()]) < 5) {
                $array[$faq->getCategory()->getName()][] = $faq;
            }
        }

        return $array;
    }

    /**
     * @Rest\Route("/{name}", name="faq_category")
     * @Rest\View(serializerGroups={"faq_light", "category"})
     * @param Category $category
     * @return array
     */
    public function showByCategory(Category $category)
    {
        return ["category" => $category,"items" => $this->getDoctrine()->getRepository(Faq::class)->findBy(["category" => $category])];
    }

    /**
     * @Rest\Route("/{name}/{id}", name="faq_show")
     * @Rest\View(serializerGroups={"faq", "category", "faq_answer"})
     * @param Category $category
     * @param Faq $faq
     * @return Faq
     */
    public function show(Faq $faq){
        return $faq;
    }
}
