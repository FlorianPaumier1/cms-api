<?php

namespace App\Controller\Frontend;

use App\Entity\Forum;
use App\Entity\ForumAnswer;
use App\Form\Frontend\ForumAnswerType;
use App\Form\Frontend\ForumType;
use App\Model\Representation\Pagination;
use App\Repository\CategoryRepository;
use App\Repository\ForumRepository;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Request\ParamFetcher;
use JMS\Serializer\SerializerInterface;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Cache\CacheItem;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Cache\CacheInterface;

/**
 * @Route("/forum")
 */
class ForumController extends AbstractController
{

    /** @var CacheInterface $cache */
    private $cache;


    public function __construct(CacheInterface $cache)
    {
        $this->cache = $cache;
    }

    /**
     * @Rest\Get("/categories", name="front_forum_list_category")
     * @Rest\View(serializerGroups={"category", "pagination"})
     * @Rest\QueryParam(name="sort", default="id", requirements="name|id")
     * @Rest\QueryParam(name="direction", default="asc", requirements="asc|desc")
     * @Rest\QueryParam(name="page", default=1, requirements="\d+")
     * @Rest\QueryParam(name="limit", default=Pagination::DEFAULT_LIMIT, requirements="\d+")
     * @param CategoryRepository $repository
     * @param PaginatorInterface $paginator
     * @param ParamFetcher $fetcher
     * @return mixed
     * @throws \Psr\Cache\InvalidArgumentException
     */
    public function index(CategoryRepository $repository, PaginatorInterface $paginator, ParamFetcher $fetcher)
    {
        return $this->cache->get("forum_index", function () use ($repository,$paginator,$fetcher) {
            return Pagination::paginate($repository
                ->createQueryBuilder("c")->where("c.type = 'forum'"), $paginator,$fetcher);
        });
    }

    /**
     * @Rest\Get("/", name="front_forum_list")
     * @Rest\QueryParam(name="category", requirements="\d+")
     * @Rest\View(serializerGroups={"forum_light", "pagination"})
     * @Rest\QueryParam(name="sort", default="id", requirements="name|id")
     * @Rest\QueryParam(name="direction", default="asc", requirements="asc|desc")
     * @Rest\QueryParam(name="page", default=1, requirements="\d+")
     * @Rest\QueryParam(name="limit", default=Pagination::DEFAULT_LIMIT, requirements="\d+")
     * @param ForumRepository $repository
     * @param PaginatorInterface $paginator
     * @param ParamFetcher $fetcher
     * @return mixed
     * @throws \Psr\Cache\InvalidArgumentException
     */
    public function list(ForumRepository $repository, PaginatorInterface $paginator, ParamFetcher $fetcher)
    {
        return $this->cache->get("forum_index", function () use ($repository,$paginator,$fetcher) {
            return Pagination::paginate($repository
                ->createQueryBuilder("f")
                ->innerJoin("f.categories", "c")
                ->where("c.id = :id")
                ->setParameter("id", $fetcher->get("category"))
                , $paginator,$fetcher);
        });
    }

    /**
     * @Route("/new", name="forum_new", methods={"GET","POST"})
     * @param Request $request
     * @param SerializerInterface $serializer
     * @return Response
     */
    public function new(Request $request, SerializerInterface $serializer): Response
    {
        $forum = new Forum();
        $form = $this->createForm(ForumType::class, $forum);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($forum);
            $entityManager->flush();

            return new JsonResponse("élément correctement créé", 201);
        }

        return new JsonResponse($serializer->serialize($form->getErrors(), 'json'), 500);
    }

    /**
     * @Route("/{id}", name="forum_show", methods={"GET"})
     * @param Forum $forum
     * @return Response
     */
    public function show(Forum $forum): Response
    {
        return $this->render('forum/show.html.twig', [
            'forum' => $forum,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="forum_edit", methods={"GET","POST"})
     * @param Request $request
     * @param Forum $forum
     * @param SerializerInterface $serializer
     * @return Response
     */
    public function edit(Request $request, Forum $forum, SerializerInterface $serializer): Response
    {
        $form = $this->createForm(ForumType::class, $forum);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return new JsonResponse("élément correctement créé", 201);
        }

        return new JsonResponse($serializer->serialize($form->getErrors(), 'json'), 500);
    }

    /**
     * @Route("/{id}", name="forum_delete", methods={"DELETE"})
     * @param Request $request
     * @param Forum $forum
     * @return Response
     */
    public function delete(Request $request, Forum $forum): Response
    {
        if ($this->isCsrfTokenValid('delete' . $forum->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($forum);
            $entityManager->flush();
        }

        return new JsonResponse("élément correctement supprimé", 203);
    }

    /**
     * @Rest\Post("/{id}/answer", name="forum_answer")
     * @param Forum $forum
     * @param Request $request
     * @param SerializerInterface $serializer
     * @return JsonResponse
     */
    public function answer(Forum $forum, Request $request, SerializerInterface $serializer)
    {
        $answer = new ForumAnswer();

        $form = $this->createForm(ForumAnswerType::class, $answer);
        $form->submit($request->request->all());

        if ($form->isValid()){

            $em = $this->getDoctrine()->getManager();
            $em->persist($answer);
            $em->flush();

            return new JsonResponse("Réponse créé", 201);
        }

        return new JsonResponse($serializer->serialize($form->getErrors(), 'json'), 500);
    }
}
