<?php

namespace App\Controller\Frontend;

use App\Entity\Article;
use App\Model\Representation\Pagination;
use App\Repository\ArticleRepository;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Request\ParamFetcher;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use OpenApi\Annotations as OA;

/**
 * @Route("/article")
 */
class ArticleController extends AbstractController
{

    /**
     * @Rest\View(serializerGroups={"article_light","date", "pagination"})
     * @Rest\Get("", name="frontend_article_index")
     * @Rest\QueryParam(name="sort", default="id", requirements="name|id")
     * @Rest\QueryParam(name="direction", default="asc", requirements="asc|desc")
     * @Rest\QueryParam(name="page", default=1, requirements="\d+")
     * @Rest\QueryParam(name="limit", default=Pagination::DEFAULT_LIMIT, requirements="\d+")
     * @OA\Get(
     *     path="/article",
     *     summary=DESCRIPTION_GET_ALL,
     *     @OA\Parameter(in="query", name="sort", description="field on which the sort is done", @OA\Schema(type="string", enum={"name","id"})),
     *     @OA\Parameter(in="query", name="direction", description="direction of the sort", @OA\Schema(type="string", enum={"asc","desc"})),
     *     @OA\Parameter(in="query", name="page", description="the page to return", @OA\Schema(type="integer")),
     *     @OA\Parameter(in="query", name="limit", description="the number of result per page", @OA\Schema(type="integer")),
     *     @OA\Response(response="200", description="SuccessFull operation"),
     *     @OA\Response(response="404", description="Not Found operation")
     * )
     * @param ArticleRepository $articleRepository
     * @param ParamFetcher $fetcher
     * @param PaginatorInterface $paginator
     * @return Pagination
     */
    public function index(ArticleRepository $articleRepository, ParamFetcher $fetcher, PaginatorInterface $paginator): Pagination
    {
        return Pagination::paginate($articleRepository->findAll(), $paginator, $fetcher);
    }

    /**
     * @Rest\View(serializerGroups={"article", "date", "user_light"})
     * @Rest\Get("/{slug}", name="frontend_article_show")
     * @Rest\QueryParam(name="chapter", default="1", requirements="\d+")
     * @OA\Get(
     *     path="/lesson/{slug}",
     *     description="show article",
     *     @OA\Response(response="200", description="SuccessFull operation"),
     *     @OA\Response(response="404", description="Not Found operation")
     * )
     * @param Article $article
     * @return Article
     */
    public function show(Article $article): Article
    {
        return $article;
    }

}
