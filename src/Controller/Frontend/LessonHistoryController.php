<?php

namespace App\Controller\Frontend;

use App\Entity\LessonHistory;
use App\Form\Frontend\LessonHistoryType;
use App\Repository\LessonHistoryRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


/**
 * @Route("/lesson/history")
 */
class LessonHistoryController extends AbstractController
{
    /**
     * @Route("/", name="lesson_history_index", methods={"GET"})
     */
    public function index(LessonHistoryRepository $lessonHistoryRepository): Response
    {
        return $this->render('lesson_history/index.html.twig', [
            'lesson_histories' => $lessonHistoryRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="lesson_history_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $lessonHistory = new LessonHistory();
        $form = $this->createForm(LessonHistoryType::class, $lessonHistory);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($lessonHistory);
            $entityManager->flush();

            return $this->redirectToRoute('lesson_history_index');
        }

        return $this->render('lesson_history/new.html.twig', [
            'lesson_history' => $lessonHistory,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="lesson_history_show", methods={"GET"})
     */
    public function show(LessonHistory $lessonHistory): Response
    {
        return $this->render('lesson_history/show.html.twig', [
            'lesson_history' => $lessonHistory,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="lesson_history_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, LessonHistory $lessonHistory): Response
    {
        $form = $this->createForm(LessonHistoryType::class, $lessonHistory);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('lesson_history_index');
        }

        return $this->render('lesson_history/edit.html.twig', [
            'lesson_history' => $lessonHistory,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="lesson_history_delete", methods={"DELETE"})
     */
    public function delete(Request $request, LessonHistory $lessonHistory): Response
    {
        if ($this->isCsrfTokenValid('delete'.$lessonHistory->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($lessonHistory);
            $entityManager->flush();
        }

        return $this->redirectToRoute('lesson_history_index');
    }
}
