<?php

namespace App\Controller\Frontend;

use App\Controller\ApiController;
use App\Entity\Lesson;
use App\Entity\LessonChapter;
use App\Entity\LessonHistory;
use App\Model\Representation\Pagination;
use App\Repository\LessonChapterRepository;
use App\Repository\LessonRepository;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Request\ParamFetcher;
use Knp\Component\Pager\PaginatorInterface;
use OpenApi\Annotations as OA;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Rest\Route("/lesson")
 */
class LessonController extends ApiController
{
    /**
     * @Rest\View(serializerGroups={"lesson_light","date", "pagination"})
     * @Rest\Get(name="lesson_index")
     * @Rest\QueryParam(name="sort", default="id", requirements="name|id")
     * @Rest\QueryParam(name="direction", default="asc", requirements="asc|desc")
     * @Rest\QueryParam(name="page", default=1, requirements="\d+")
     * @Rest\QueryParam(name="limit", default=Pagination::DEFAULT_LIMIT, requirements="\d+")
     * @OA\Get(
     *     path="/lesson",
     *     summary=DESCRIPTION_GET_ALL,
     *     @OA\Parameter(in="query", name="sort", description="field on which the sort is done", @OA\Schema(type="string", enum={"name","id"})),
     *     @OA\Parameter(in="query", name="direction", description="direction of the sort", @OA\Schema(type="string", enum={"asc","desc"})),
     *     @OA\Parameter(in="query", name="page", description="the page to return", @OA\Schema(type="integer")),
     *     @OA\Parameter(in="query", name="limit", description="the number of result per page", @OA\Schema(type="integer")),
     *     @OA\Response(response="200", description="SuccessFull operation"),
     *     @OA\Response(response="404", description="Not Found operation")
     * )
     * @param LessonRepository $lessonRepository
     * @param ParamFetcher $paramFetcher
     * @param PaginatorInterface $paginator
     * @return Pagination
     */
    public function index(LessonRepository $lessonRepository, ParamFetcher $paramFetcher, PaginatorInterface $paginator)
    {
        return Pagination::paginate($lessonRepository->createQueryBuilder("l"), $paginator, $paramFetcher);
    }

    /**
     * @Rest\View(serializerGroups={"lesson", "lesson_chapter", "chapter_light"})
     * @Rest\Get("/{slug}", name="lesson_show")
     * @OA\Get(
     *     path="/lesson/{slug}",
     *     description="show lesson",
     *     @OA\Response(response="200", description="SuccessFull operation"),
     *     @OA\Response(response="404", description="Not Found operation")
     * )
     * @param Lesson $lesson
     * @return Lesson
     */
    public function show(Lesson $lesson)
    {
        $this->setHistory($lesson);

        return $lesson;
    }

    /**
     * @Rest\Get("/{slug}/chapter/{id}")
     * @Rest\View(serializerGroups={"chapter", "chapter_block", "lesson_light"})
     * @OA\Get(
     *     path="/lesson/{slug}/chapter/{chapter}",
     *     tags={"Lesson"},
     *     summary=DESCRIPTION_GET_ALL,
     *     @OA\Parameter(in="query", name="sort", description="field on which the sort is done", @OA\Schema(type="string", enum={"name","id"})),
     *     @OA\Parameter(in="query", name="direction", description="direction of the sort", @OA\Schema(type="string", enum={"asc","desc"})),
     *     @OA\Parameter(in="query", name="page", description="the page to return", @OA\Schema(type="integer")),
     *     @OA\Parameter(in="query", name="limit", description="the number of result per page", @OA\Schema(type="integer")),
     *     @OA\Response(response="200", description=DESCRIPTION_RESPONSE_200, @OA\JsonContent(type="array", @OA\Items(ref="#/components/schemas/City")))
     * )
     * @param Lesson $lesson
     * @param LessonChapter $lessonChapter
     * @return LessonChapter
     */
    public function chapter(Lesson $lesson,LessonChapter $lessonChapter)
    {
        $this->setHistory($lesson, $lessonChapter->getPosition());
        return $lessonChapter;
    }

    /**
     * @param Lesson $lesson
     * @param int $chapter
     */
    private function setHistory(Lesson $lesson, int $chapter = 0)
    {
        if ($this->getUser()) {
            $em = $this->getDoctrine()->getManager();
            $history = $em->getRepository(LessonHistory::class)->findOneBy(["user" => $this->getUser(), "lesson" => $lesson]);

            if (!$history) {
                $history = (new LessonHistory())
                    ->setUser($this->getUser())
                    ->setLesson($lesson)
                    ->setIsOver(false);
            }

            $history->setChapter($chapter);

            $this->getDoctrine()->getManager()->persist($history);
            $this->getDoctrine()->getManager()->flush();
        }
    }
}
