<?php


namespace App\Controller\Frontend;


use App\Controller\ApiController;
use App\Entity\Sondage;
use App\Entity\SondageAnswer;
use App\Entity\SondageQuery;
use App\Model\Representation\Pagination;
use App\Repository\SondageRepository;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Request\ParamFetcher;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;


/**
 * @Rest\Route("/sondage")
 * Class SondageController
 * @package App\Form\Frontend
 */
class SondageController extends ApiController
{

    /**
     * @Rest\Get(name="frontend_sondage")
     * @param SondageRepository $sondageRepository
     * @param PaginatorInterface $paginator
     * @param ParamFetcher $fetcher
     * @return Pagination
     */
    public function index(SondageRepository $sondageRepository, PaginatorInterface $paginator, ParamFetcher $fetcher)
    {
        return Pagination::paginate($sondageRepository->findAll(), $paginator, $fetcher);
    }

    /**
     * @Rest\Post(name="front_sondage_answer")
     * @Rest\RequestParam(name="query",requirements="\d+")
     * @param ParamFetcher $fetcher
     * @return JsonResponse
     * @return JsonResponse
     */
    public function answer(Request $request, ParamFetcher $fetcher)
    {
        $query = $this->getDoctrine()->getRepository(SondageQuery::class)->find($fetcher->get("query"));

        if (!$query) {
            return new JsonResponse("La question n'existe pas", 404);
        }

        if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
            $ip = $_SERVER['HTTP_CLIENT_IP'];
        } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        } else {
            $ip = $_SERVER['REMOTE_ADDR'];
        }

        $answer = (new SondageAnswer())
            ->setIpAddress($ip)
            ->setSonageQuery($query);


        $this->getDoctrine()->getManager()->persist($answer);
        $this->getDoctrine()->getManager()->flush();

        return new JsonResponse("Votre réponse à bien été prise en compte", 201);
    }

    /**
     * @Rest\Get("/{id}", name="frontend_show_result")
     * @Rest\View(serializerGroups={"sondage", "sondage_query"})
     * @param Sondage $sondage
     * @return Sondage
     */
    public function show(Sondage $sondage)
    {
        return $sondage;
    }
}
