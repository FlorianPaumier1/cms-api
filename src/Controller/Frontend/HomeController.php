<?php


namespace App\Controller\Frontend;


use App\Entity\User;
use App\Form\Frontend\UserRegisterType;
use App\Repository\ArticleRepository;
use App\Repository\LessonRepository;
use App\Repository\TestimonyRepository;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractController
{

    /**
     * @Route("/home", name="app_frontend_index")
     * @Rest\View(serializerGroups={"lesson_home", "article_light", "testimony"})
     * @param Request $request
     * @param TestimonyRepository $testimonyRepository
     * @param LessonRepository $lessonRepository
     * @param ArticleRepository $articleRepository
     * @return array
     */
    public function index(
        Request $request,
        TestimonyRepository $testimonyRepository,
        LessonRepository $lessonRepository,
        ArticleRepository $articleRepository
    ) {
        $lastLessons = $lessonRepository->createQueryBuilder("l")->setMaxResults(5)->orderBy("l.createdAt", "ASC")->getQuery()->getResult();
        $lastArticles = $articleRepository->createQueryBuilder("a")->setMaxResults(5)->orderBy("a.createdAt", "ASC")->getQuery()->getResult();
        $testimonies = $testimonyRepository->createQueryBuilder("t")->setMaxResults(10)->orderBy("t.createdAt", "ASC")->getQuery()->getResult();

        return [
            "lessons" => ["data" => $lastLessons, "url" => "/lesson/"],
            "articles" => ["data" => $lastArticles, "url" => "/article/"],
            "testimonies" => ["data" => $testimonies]
        ];
    }
}
