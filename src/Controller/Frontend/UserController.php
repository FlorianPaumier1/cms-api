<?php


namespace App\Controller\Frontend;


use App\Annotations\ResponseAccessDenied;
use App\Controller\ApiController;
use App\Entity\User;
use App\Form\Frontend\UserType;
use App\Model\Representation\Pagination;
use App\Repository\ContactRepository;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Request\ParamFetcher;
use JMS\Serializer\SerializerInterface;
use Knp\Component\Pager\PaginatorInterface;
use OpenApi\Annotations as OA;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @Rest\Route("/user")
 * Class UserController
 * @package App\Controller\Frontend
 */
class UserController extends ApiController
{

    /**
     * @Rest\Get("/", name="app_frontend_user_profile")
     * @Rest\View(serializerGroups={"detail"})
     * @OA\Get(
     *     path="/user/profile",
     *     description="User profile",
     *     @OA\Response(response="200", description="Successful operation")
     * )
     * @param Request $request
     * @param SerializerInterface $serializer
     * @return object|UserInterface|null
     */
    public function profile(Request $request, SerializerInterface $serializer)
    {
        return $this->getUser();
    }

    /**
     * @Rest\Get("/contact")
     * @Rest\View(serializerGroups={"list", "pagination"})
     * @Rest\QueryParam(name="sort", default="id", requirements="name|id")
     * @Rest\QueryParam(name="direction", default="asc", requirements="asc|desc")
     * @Rest\QueryParam(name="page", default=1, requirements="\d+")
     * @Rest\QueryParam(name="limit", default=Pagination::DEFAULT_LIMIT, requirements="\d+")
     * @OA\Get(
     *     path="/user/contact",
     *     tags={"Contact"},
     *     summary=DESCRIPTION_GET_ALL,
     *     @OA\Parameter(in="query", name="sort", description="field on which the sort is done", @OA\Schema(type="string", enum={"name","id"})),
     *     @OA\Parameter(in="query", name="direction", description="direction of the sort", @OA\Schema(type="string", enum={"asc","desc"})),
     *     @OA\Parameter(in="query", name="page", description="the page to return", @OA\Schema(type="integer")),
     *     @OA\Parameter(in="query", name="limit", description="the number of result per page", @OA\Schema(type="integer")),
     *     @OA\Response(response="200", description=DESCRIPTION_RESPONSE_200)
     * )
     * @param ContactRepository $contactRepository
     * @param ParamFetcher $paramFetcher
     * @param PaginatorInterface $pagination
     * @return Pagination
     */
    public function contactUser(ContactRepository $contactRepository, ParamFetcher $paramFetcher, PaginatorInterface $pagination)
    {
        return Pagination::paginate($contactRepository->createQueryBuilder("c")->where("c.author")->setParameter("author", $this->getUser()), $pagination, $paramFetcher);
    }

    /**
     * @Rest\Post
     * @OA\Post(
     *     path="/user",
     *     tags={"City"},
     *     summary=DESCRIPTION_POST,
     *     @OA\RequestBody(required=true),
     *     @OA\Response(response="201", description=DESCRIPTION_RESPONSE_201),
     *     @OA\Response(response="400", description=DESCRIPTION_RESPONSE_400),
     *     @OA\Response(response="401", description=DESCRIPTION_RESPONSE_401),
     *     @OA\Response(response="403", description=DESCRIPTION_RESPONSE_403)
     * )
     * @param Request $request
     * @param SerializerInterface $serializer
     * @return JsonResponse
     */
    public function edit(Request $request, SerializerInterface $serializer)
    {
        $user = $this->getUser();
        $form = $this->createForm(UserType::class, $user);

        $this->submit($form, $request);

        if ($form->isValid()) {
            $this->getDoctrine()->getManager()->persist($user);
            $this->getDoctrine()->getManager()->flush();

            return new JsonResponse("Votre profil a bien été modifié", 200);
        }

        return new JsonResponse($serializer->serialize(["errors" => $form->getErrors()], 'json'), 500);
    }

    /**
     * @Rest\Post("/delete")
     * @param Request $request
     * @param User $user
     * @return JsonResponse
     */
    public function delete()
    {
        /** @var User $user */
        $user = $this->getUser();
        $user->setIsEnabled(false);
        $this->getDoctrine()->getManager()->persist($user);
        $this->getDoctrine()->getManager()->flush();

        return new JsonResponse("Votre compte a bien été désactivé", 204);
    }
}
