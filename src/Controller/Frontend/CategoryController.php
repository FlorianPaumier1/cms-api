<?php

namespace App\Controller\Frontend;

use App\Controller\ApiController;
use App\Entity\Category;
use App\Form\Frontend\CategoryType;
use App\Model\Representation\Pagination;
use App\Repository\CategoryRepository;
use App\Repository\LessonRepository;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Request\ParamFetcher;
use Knp\Component\Pager\PaginatorInterface;
use OpenApi\Annotations as OA;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Rest\Route("/category")
 */
class CategoryController extends ApiController
{
    /**
     * @Rest\Get("/", name="category_index")
     * @Rest\View(serializerGroups={"category", "pagination"})
     * @Rest\QueryParam(name="sort", default="id", requirements="name|id")
     * @Rest\QueryParam(name="direction", default="asc", requirements="asc|desc")
     * @Rest\QueryParam(name="page", default=1, requirements="\d+")
     * @Rest\QueryParam(name="limit", default=Pagination::DEFAULT_LIMIT, requirements="\d+")
     * @Rest\QueryParam(name="type", default="article", requirements="\w+")
     * @OA\Get(
     *     path="/category",
     *     summary=DESCRIPTION_GET_ALL,
     *     @OA\Parameter(in="query", name="sort", description="field on which the sort is done", @OA\Schema(type="string", enum={"name","id"})),
     *     @OA\Parameter(in="query", name="direction", description="direction of the sort", @OA\Schema(type="string", enum={"asc","desc"})),
     *     @OA\Parameter(in="query", name="page", description="the page to return", @OA\Schema(type="integer")),
     *     @OA\Parameter(in="query", name="limit", description="the number of result per page", @OA\Schema(type="integer")),
     *     @OA\Response(response="200", description="SuccessFull operation"),
     *     @OA\Response(response="404", description="Not Found operation")
     * )
     * @param CategoryRepository $categoryRepository
     * @param ParamFetcher $paramFetcher
     * @return Pagination
     */
    public function index(CategoryRepository $categoryRepository, ParamFetcher $paramFetcher)
    {
        return $categoryRepository->createQueryBuilder("c")
            ->where("c.type = :type")
            ->setParameter("type", $paramFetcher->get("type"))->getQuery()->getResult();
    }
}
