<?php


namespace App\Controller\Frontend;


use App\Controller\ApiController;
use App\Entity\User;
use App\Form\Common\UserLoginType;
use App\Form\Frontend\UserRegisterType;
use App\Service\MailerService;
use Doctrine\ORM\EntityManagerInterface;
use FOS\RestBundle\Controller\Annotations as Rest;
use JMS\Serializer\SerializerInterface;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface;
use OpenApi\Annotations as OA;
use PHPUnit\Util\Json;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Mailer\MailerInterface;

/**
 * @Rest\Route("/auth")
 * Class SecurityController
 * @package App\Controller\Frontend
 */
class SecurityController extends ApiController
{

    /**
     * @OA\Post(
     *     path="/auth/register",
     *     @OA\Response(response="201", description="Successful operation")
     * )
     * @Rest\Route("/register", name="app_frontend_register", methods={"POST"})
     * @param Request $request
     * @return JsonResponse
     */
    public function register(Request $request, SerializerInterface $serializer, MailerInterface $mailer)
    {
        $user = new User();

        $form = $this->createForm(UserRegisterType::class, $user);
        $this->submit($form, $request);

        if ($form->isValid()) {
            if ($this->getDoctrine()->getRepository(User::class)->findOneBy(["username" => $user->getUsername()])) {
                return new JsonResponse("Votre username existe déjà. Veuilliez le modifier", 500);
            } elseif ($this->getDoctrine()->getRepository(User::class)->findOneBy(["email" => $user->getEmail()])) {
                return new JsonResponse("Votre email existe déjà. Veuilliez vous connecter.", 500);
            }
            $user->setConfirmationToken($this->generateToken());

            $this->getDoctrine()->getManager()->persist($user);
            $this->getDoctrine()->getManager()->flush();

            $mail = (new MailerService($mailer))->confirmationUser($user);

            if ($mail["error"]) {
                return (new JsonResponse($mail["message"]))->setStatusCode(500);
            }

            return new JsonResponse("Vous avez bien été enregistrer. Veuillez confirmez votre email avant de vous connectez.", 201);
        }

        return new JsonResponse(["errors" => $serializer->serialize($form->getErrors(), 'json')], 500);
    }

    /**
     * @OA\Post(
     *     path="/users/confirmation/{token}",
     *     @OA\Response(response="200", description="Successful operation")
     * )
     * @Rest\Route("/users/confirmation/{token}", name="app_frontend_confirmation_account")
     * @param Request $request
     */
    public function confirmationAccount(Request $request, EntityManagerInterface $em)
    {
        $token = $request->attributes->get("token");

        $user = $em->getRepository(User::class)->findOneBy(["confirmationToken" => $token]);

        if (!$user) {
            return new JsonResponse("Le token n'est pas valide", 500);
        }

        return $this->redirect("http://localhost:280");
    }

    /**
     * @Rest\Get("/token", name="app_generate_token")
     */
    public function confirmationToken()
    {
        return new JsonResponse("Hello World");
    }
}
