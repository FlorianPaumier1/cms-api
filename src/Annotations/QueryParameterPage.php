<?php declare(strict_types=1);

namespace App\Annotations;

use OpenApi\Annotations\Parameter;
use OpenApi\Annotations\Schema;

/**
 * @Annotation
 */
class QueryParameterPage extends Parameter
{
    public $in = 'query';

    public $name = 'direction';

    public $description = 'direction of the sort';
}
