<?php declare(strict_types=1);

namespace App\Annotations;

use OpenApi\Annotations\Response;

/**
 * @Annotation
 */
class ResponseOk extends Response
{
    public $response = '200';

    public $description = 'Successful operation';
}
