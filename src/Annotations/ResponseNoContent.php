<?php declare(strict_types=1);

namespace App\Annotations;

use OpenApi\Annotations\Response;

/**
 * @Annotation
 */
class ResponseNoContent extends Response
{
    public $response = '204';

    public $description = 'No content';
}
