<?php declare(strict_types=1);

namespace App\Annotations;

use OpenApi\Annotations\Parameter;

/**
 * @Annotation
 */
class QueryParameterLimit extends Parameter
{
    public $in = 'query';

    public $name = 'limit';

    public $description = 'the number of result per page';
}
