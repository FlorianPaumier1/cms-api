<?php declare(strict_types=1);

namespace App\Annotations;

use OpenApi\Annotations\Parameter;

/**
 * @Annotation
 */
class QueryParameterFilter extends Parameter
{
    public $in = 'query';

    public $name = 'filters';

    public $description = 'filter the results';
}
