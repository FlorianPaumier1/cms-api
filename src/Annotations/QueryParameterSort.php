<?php declare(strict_types=1);

namespace App\Annotations;

use OpenApi\Annotations\Parameter;

/**
 * @Annotation
 */
class QueryParameterSort extends Parameter
{
    public $in = 'query';

    public $name = 'page';

    public $description = 'the page to return';
}
