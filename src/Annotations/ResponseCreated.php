<?php declare(strict_types=1);

namespace App\Annotations;

use OpenApi\Annotations\Response;

/**
 * @Annotation
 */
class ResponseCreated extends Response
{
    public $response = '201';

    public $description = 'Resource created';
}
