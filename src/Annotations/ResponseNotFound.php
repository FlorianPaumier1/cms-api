<?php declare(strict_types=1);

namespace App\Annotations;

use OpenApi\Annotations\Response;

/**
 * @Annotation
 */
class ResponseNotFound extends Response
{
    public $response = '404';
    
    public $description = 'Resource not found';
}
