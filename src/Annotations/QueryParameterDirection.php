<?php declare(strict_types=1);

namespace App\Annotations;

use OpenApi\Annotations\Parameter;

/**
 * @Annotation
 */
class QueryParameterDirection extends Parameter
{
    public $in = 'query';

    public $name = 'direction';

    public $description = 'direction of the sort';
}
