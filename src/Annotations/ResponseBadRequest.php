<?php declare(strict_types=1);

namespace App\Annotations;

use OpenApi\Annotations\Response;

/**
 * @Annotation
 */
class ResponseBadRequest extends Response
{
    public $response = '400';

    public $description = 'Bad request';
}
