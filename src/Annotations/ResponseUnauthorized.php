<?php declare(strict_types=1);

namespace App\Annotations;

use OpenApi\Annotations\Response;

/**
 * @Annotation
 */
class ResponseUnauthorized extends Response
{
    public $response = '401';
    
    public $description = 'Unauthorized';
}
