<?php declare(strict_types=1);

namespace App\Annotations;

use OpenApi\Annotations\Response;

/**
 * @Annotation
 */
class ResponseAccessDenied extends Response
{
    public $response = '403';
    
    public $description = 'Access denied';
}
