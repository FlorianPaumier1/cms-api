<?php

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class UserFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {

        for ($i = 0; $i < 50; $i++) {
            $int = mt_rand(1589839200, 1600529011);

            $user = (new User())
                ->setPlainPassword("root")
                ->setEmail("user$i@cms.com")
                ->setUsername("user $i")
                ->setIsConfirmed(true)
                ->setIsEnabled(true)
                ->setConfirmedAt(new \DateTime())
                ;

            $manager->persist($user);
            $user->createdAt((new \DateTime())->setTimestamp($int));
            $manager->persist($user);
        }

        $admin = (new User())
            ->setPlainPassword("root")
            ->setEmail("admin@cms.com")
            ->setUsername("admin")
            ->setIsConfirmed(true)
            ->setIsEnabled(true)
            ->setRoles(["ROLE_ADMIN"])
            ->setCreatedAt((new \DateTime()))
            ->setConfirmedAt(new \DateTime());

        $manager->persist($admin);
        $manager->flush();
    }
}
