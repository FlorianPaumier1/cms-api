<?php

namespace App\DataFixtures;

use App\Entity\Category;
use App\Entity\Faq;
use App\Entity\FaqAnswer;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;

class FaqFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        /** @var Category $categories */
        $categories = $manager->getRepository(Category::class)->findAll();

        /** @var User $user */
        $user = $manager->getRepository(User::class)->findOneBy(["username" => "admin"]);

        $faker = Factory::create();

        foreach ($categories as $category) {
            for ($i = 0; $i < 7; $i++) {
                $faq = (new Faq())
                    ->setDescription($faker->paragraph(5))
                    ->setTitle(join(" ", $faker->words(4)))
                    ->setIsEnabled(true)
                    ->setCategory($category)
                    ->setAuthor($user);

                for ($y=0; $y < 3; $y++){
                    $answer = (new FaqAnswer())
                        ->setQuery(join(" ", $faker->words))
                        ->setAnswer($faker->sentence())
                        ->setFaq($faq)
                    ;

                    $manager->persist($answer);
                }
                $manager->persist($faq);
            }
        }

        $manager->flush();
    }

    public function getDependencies()
    {
        return [
            CategoryFixtures::class
        ];
    }
}
