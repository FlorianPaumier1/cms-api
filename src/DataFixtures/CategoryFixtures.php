<?php

namespace App\DataFixtures;

use App\Entity\Category;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;

class CategoryFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $faker = Factory::create();

        $types = [];
        $typesSpecial = ["article", "lesson"];

        for ($i=0; $i<5;$i++){
            $types[] = $faker->word;
        }

        for ($i=0; $i<10; $i++){
            $category = (new Category())
                ->setName($faker->word)
            ->settype($types[rand(0,4)]);

            $manager->persist($category);
        }

        for ($i=0; $i<10; $i++){
            $category = (new Category())
                ->setName($faker->word)
            ->settype($typesSpecial[rand(0,1)]);

            $manager->persist($category);
        }

        $manager->flush();
    }
}
