<?php

namespace App\DataFixtures;

use App\Entity\Lesson;
use App\Entity\LessonChapter;
use App\Entity\LessonChapterBlock;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;

class LessonFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        /** @var User $user */
        $user = $manager->getRepository(User::class)->findOneBy(["username" => "admin"]);

        $faker = Factory::create();
        $lessons = [];
        $lessonsChapters = [];

        for ($i = 0; $i < 50; $i++) {
            $int = mt_rand(1589839200, 1600529011);
            $lesson = (new Lesson())
                ->setAuthor($user)
                ->setDescription("Description pour le cours numéro ${i}")
                ->setTitle("Lesson numéro ${i}")
                ->setSlug("lesson-numero-${i}")
                ->setThumbnail($faker->imageUrl(640, 480, 'animals', true, 'Faker'));


            $manager->persist($lesson);
            $lesson->createdAt((new \DateTime())->setTimestamp($int));
            $manager->persist($lesson);

            $lessons[] = $lesson;
        }


        foreach ($lessons as $lesson) {
            for ($i = 0; $i < 5; $i++) {
                $lessonsChapters[] = (new LessonChapter())
                    ->setTitle("Chapter ${i}")
                    ->setSlug("chapter-${i}")
                    ->setLesson($lesson)
                    ->setPosition($i + 1);
            }
            $manager->persist($lesson);
        }

        $faker = Factory::create();

        foreach ($lessonsChapters as $lessonsChapter) {
            for ($i = 0; $i < 3; $i++) {
                $block = (new LessonChapterBlock())
                    ->setPosition($i + 1)
                    ->setTitle("Block ${i}")
                    ->setContent($faker->paragraph(20, true))
                    ->setLessonChapter($lessonsChapter);
                $manager->persist($block);
            }

            $manager->persist($lessonsChapter);
        }


        $manager->flush();
    }

    public function getDependencies()
    {
        return [
            UserFixtures::class,
            CategoryFixtures::class
        ];
    }
}
