<?php

namespace App\DataFixtures;

use App\Entity\Article;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;

class ArticleFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager)
    {

        /** @var User $user */
        $user = $manager->getRepository(User::class)->findOneBy(["username" => "admin"]);

        $faker = Factory::create();

        for ($i = 0; $i < 100; $i++) {
            $int = mt_rand(1589839200, 1600529011);

            $article = (new Article())
                ->setTitle("Article $i")
                ->setSlug("article-$i")
                ->setAuthor($user)
                ->setContent($faker->paragraph(15, true))
                ->setIsVisible(1)
                ->setThumbnail($faker->imageUrl(640, 480, 'animals', true, 'Faker'))
            ;

            $manager->persist($article);

            $article->createdAt((new \DateTime())->setTimestamp($int));
            $manager->persist($article);
        }

        $manager->flush();
    }

    public function getDependencies()
    {
        return [
            UserFixtures::class,
            CategoryFixtures::class
        ];
    }
}
