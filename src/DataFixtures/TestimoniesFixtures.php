<?php

namespace App\DataFixtures;

use App\Entity\Testimony;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;

class TestimoniesFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {

        $faker = Factory::create();

        for ($i = 0; $i < 50; $i++) {
            $testimony = (new Testimony())
                ->setName($faker->name)
                ->setContent($faker->sentence)
                ->createdAt($faker->dateTimeBetween("-1 year", "+1 year"))
            ;

            $manager->persist($testimony);
        }

        $manager->flush();
    }
}
