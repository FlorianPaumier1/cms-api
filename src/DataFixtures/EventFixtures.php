<?php

namespace App\DataFixtures;

use App\Entity\Event;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;

class EventFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        /** @var User $user */
        $user = $manager->getRepository(User::class)->findOneBy(["username" => "admin"]);

        $faker = Factory::create();

        for ($i = 0; $i < 50; $i++) {
            $event = (new Event())
                ->setAuthor($user)
                ->setTitle("Event $i")
                ->setSlug("event-$i")
                ->setDescription($faker->paragraph)
                ->setTakePlaceAt($faker->dateTimeBetween("-1 year", "+1 year"))
                ->setPublishedAt(new \DateTime())
            ;

            $manager->persist($event);
        }

        $manager->flush();
    }

    public function getDependencies()
    {
        return [
            UserFixtures::class
        ];
    }
}
