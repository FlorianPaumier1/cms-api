<?php

namespace App\Entity;

use App\Repository\ForumAnswerRepository;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;

/**
 * @Serializer\ExclusionPolicy("all")
 * @ORM\Entity(repositoryClass=ForumAnswerRepository::class)
 * @ORM\HasLifecycleCallbacks()
 */
class ForumAnswer
{

    use TimestampEntity;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private ?int $id;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="forumAnswers")
     */
    private ?User $author;

    /**
     * @ORM\Column(type="text")
     */
    private ?string $content;

    /**
     * @ORM\Column(type="boolean")
     */
    private ?bool $isVisible;

    /**
     * @var boolean
     * @ORM\Column(type="boolean")
     */
    private ?bool $isEdited;

    /**
     * @ORM\ManyToOne(targetEntity=Forum::class, inversedBy="forumAnswers")
     */
    private $forum;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getAuthor(): ?User
    {
        return $this->author;
    }

    public function setAuthor(?User $author): self
    {
        $this->author = $author;

        return $this;
    }

    public function getContent(): ?string
    {
        return $this->content;
    }

    public function setContent(string $content): self
    {
        $this->content = $content;

        return $this;
    }

    public function getIsVisible(): ?bool
    {
        return $this->isVisible;
    }

    public function setIsVisible(bool $isVisible): self
    {
        $this->isVisible = $isVisible;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getIsEdited(): ?bool
    {
        return $this->isEdited;
    }

    /**
     * @param mixed $isEdited
     */
    public function setIsEdited(bool $isEdited): ForumAnswer
    {
        $this->isEdited = $isEdited;
        return $this;
    }

    public function getForum(): ?Forum
    {
        return $this->forum;
    }

    public function setForum(?Forum $forum): self
    {
        $this->forum = $forum;

        return $this;
    }
}
