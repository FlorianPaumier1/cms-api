<?php

namespace App\Entity;

use App\Repository\EventInscriptionRepository;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;

/**
 * @Serializer\ExclusionPolicy("all")
 * @ORM\Entity(repositoryClass=EventInscriptionRepository::class)
 * @ORM\HasLifecycleCallbacks()
 */
class EventInscription
{

    use TimestampEntity;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="eventInscriptions")
     * @ORM\JoinColumn(nullable=false)
     */
    private $user;

    /**
     * @Serializer\Expose()
     * @Serializer\Groups({"event_inscription_light"})
     * @ORM\ManyToOne(targetEntity=Event::class, inversedBy="eventInscriptions")
     * @ORM\JoinColumn(nullable=false)
     */
    private $event;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getEvent(): ?Event
    {
        return $this->event;
    }

    public function setEvent(?Event $event): self
    {
        $this->event = $event;

        return $this;
    }
}
