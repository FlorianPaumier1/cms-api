<?php

namespace App\Entity;

use App\Repository\SondageQueryRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;

/**
 * @JMS\ExclusionPolicy("all")
 * @ORM\Entity(repositoryClass=SondageQueryRepository::class)
 * @ORM\HasLifecycleCallbacks()
 */
class SondageQuery
{

    use TimestampEntity;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private ?int $id;

    /**
     * @JMS\Expose()
     * @JMS\Groups({"sondage_query", "sondage_query_light"})
     * @ORM\Column(type="string", length=255)
     */
    private ?string $query;

    /**
     * @JMS\Expose()
     * @JMS\Groups({"sondage_query"})
     * @ORM\ManyToOne(targetEntity=Sondage::class, inversedBy="sondageQueries", cascade={"persist", "remove"})
     */
    private ?Sondage $sondage;

    /**
     * @ORM\OneToMany(targetEntity=SondageAnswer::class, mappedBy="sonageQuery")
     */
    private Collection $sondageAnswers;

    /**
     * @JMS\Expose()
     * @JMS\Groups({"sondage_query"})
     * @var int
     */
    private int $answerCount;

    public function __construct()
    {
        $this->sondageAnswers = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getQuery(): ?string
    {
        return $this->query;
    }

    public function setQuery(string $query): self
    {
        $this->query = $query;

        return $this;
    }

    public function getSondage(): ?Sondage
    {
        return $this->sondage;
    }

    public function setSondage(?Sondage $sondage): self
    {
        $this->sondage = $sondage;

        return $this;
    }

    /**
     * @return Collection
     */
    public function getSondageAnswers(): Collection
    {
        return $this->sondageAnswers;
    }

    public function addSondageAnswer(SondageAnswer $sondageAnswer): self
    {
        if (!$this->sondageAnswers->contains($sondageAnswer)) {
            $this->sondageAnswers[] = $sondageAnswer;
            $sondageAnswer->setSonageQuery($this);
        }

        return $this;
    }

    public function removeSondageAnswer(SondageAnswer $sondageAnswer): self
    {
        if ($this->sondageAnswers->contains($sondageAnswer)) {
            $this->sondageAnswers->removeElement($sondageAnswer);
            // set the owning side to null (unless already changed)
            if ($sondageAnswer->getSonageQuery() === $this) {
                $sondageAnswer->setSonageQuery(null);
            }
        }

        return $this;
    }

    /**
     * @JMS\PreSerialize()
     */
    public function setAnswerCount(){
        $this->answerCount = $this->sondageAnswers->count();
        return $this;
    }

    /**
     * @return int
     */
    public function getAnswerCount(): int
    {
        return $this->answerCount;
    }
}
