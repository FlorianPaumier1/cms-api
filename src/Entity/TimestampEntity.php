<?php


namespace App\Entity;


use DateTimeImmutable;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;

/**
 * @Serializer\ExclusionPolicy("none")
 * Trait TimestampEntity
 * @package App\Entity
 * @ORM\HasLifecycleCallbacks()
 */
trait TimestampEntity
{

    /**
     * @Serializer\Expose()
     * @Serializer\Groups({"date"})
     * @var \DateTime
     * @ORM\Column(type="datetime")
     */
    protected \DateTime $createdAt;

    /**
     * @Serializer\Expose()
     * @Serializer\Groups({"date"})
     * @var \DateTime
     * @ORM\Column(type="datetime")
     */
    protected \DateTime $updatedAt;

    /**
     * @return DateTimeImmutable
     */
    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    /**
     * @ORM\PrePersist()
     * @param DateTimeImmutable $createdAt
     */
    public function setCreatedAt(): self
    {
        $this->createdAt = new \DateTime();
        return $this;
    }


    /**
     * @param DateTimeImmutable $createdAt
     */
    public function createdAt(\DateTime $dateTime): self
    {
        $this->createdAt = $dateTime;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     * @param \DateTime $updatedAt
     */
    public function setUpdatedAt(): self
    {
        $this->updatedAt = new \DateTime();
        return $this;
    }


}
