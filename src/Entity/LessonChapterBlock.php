<?php

namespace App\Entity;

use App\Repository\LessonChapterBlockRepository;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;

/**
 * @Serializer\ExclusionPolicy("all")
 * @ORM\Entity(repositoryClass=LessonChapterBlockRepository::class)
 * @ORM\HasLifecycleCallbacks()
 */
class LessonChapterBlock
{

    use TimestampEntity;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=LessonChapter::class, inversedBy="lessonChapterBlocks")
     * @ORM\JoinColumn(nullable=false)
     */
    private $LessonChapter;

    /**
     * @Serializer\Expose()
     * @Serializer\Groups({"chapter_block_light", "chapter_block"})
     * @ORM\Column(type="string", length=255)
     */
    private $title;

    /**
     * @Serializer\Expose()
     * @Serializer\Groups({"chapter_block"})
     * @ORM\Column(type="text")
     */
    private $content;

    /**
     * @Serializer\Expose()
     * @Serializer\Groups({"chapter_block_light", "chapter_block"})
     * @ORM\Column(type="integer")
     */
    private $position;


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLessonChapter(): ?LessonChapter
    {
        return $this->LessonChapter;
    }

    public function setLessonChapter(?LessonChapter $LessonChapter): self
    {
        $this->LessonChapter = $LessonChapter;

        return $this;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getContent(): ?string
    {
        return $this->content;
    }

    public function setContent(string $content): self
    {
        $this->content = $content;

        return $this;
    }

    public function getPosition(): ?int
    {
        return $this->position;
    }

    public function setPosition(int $position): self
    {
        $this->position = $position;

        return $this;
    }
}
