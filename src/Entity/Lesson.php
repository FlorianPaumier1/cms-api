<?php

namespace App\Entity;

use App\Entity\Backend\Evaluation\Evaluation;
use App\Repository\LessonRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;
use JMS\Serializer\Annotation\MaxDepth;

/**
 * @Serializer\AccessorOrder("custom", custom = {"id","title","slug", "author","categories", "createdAt"})
 * @Serializer\ExclusionPolicy("ALL")
 * @ORM\Entity(repositoryClass=LessonRepository::class)
 * @ORM\HasLifecycleCallbacks()
 */
class Lesson
{

    use TimestampEntity;

    /**
     * @ORM\Id()
     * @Serializer\Expose()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @Serializer\Expose()
     * @Serializer\Groups({"lesson_light", "lesson", "lesson_home"})
     * @ORM\Column(type="string", length=255)
     */
    private $title;

    /**
     * @Serializer\Expose()
     * @Serializer\Groups({"lesson_light", "lesson", "lesson_home"})
     * @ORM\Column(type="string", length=255)
     */
    private $slug;

    /**
     * @Serializer\Expose()
     * @Serializer\Groups({"lesson_light", "lesson", "lesson_home"})
     * @ORM\Column(type="text")
     */
    private $description;

    /**
     * @Serializer\Expose()
     * @Serializer\Groups({"lesson_light", "lesson", "lesson_home"})
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $thumbnail;

    /**
     * @Serializer\Expose()
     * @Serializer\Groups({"lesson"})
     * @Serializer\MaxDepth(1)
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="lessons")
     * @ORM\JoinColumn(nullable=false)
     */
    private $author;

    /**
     * @Serializer\Expose()
     * @Serializer\Groups({"lesson_light", "lesson"})
     * @Serializer\MaxDepth(2)
     * @ORM\OneToMany(targetEntity=LessonChapter::class, mappedBy="lesson", orphanRemoval=true)
     */
    private $lessonChapters;

    /**
     * @Serializer\Expose()
     * @Serializer\Groups({"lesson_light", "lesson", "lesson_home"})
     * @Serializer\MaxDepth(1)
     * @ORM\ManyToMany(targetEntity=Category::class, mappedBy="lessons")
     */
    private $categories;

    /**
     * @ORM\OneToMany(targetEntity=LessonHistory::class, mappedBy="lesson")
     */
    private $lessonHistories;

    /**
     * @MaxDepth(1)
     * @ORM\OneToOne(targetEntity=Evaluation::class, mappedBy="lesson", cascade={"persist", "remove"})
     */
    private $evaluation;

    public function __construct()
    {
        $this->lessonChapters = new ArrayCollection();
        $this->categories = new ArrayCollection();
        $this->lessonHistories = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug(string $slug): self
    {
        $this->slug = $slug;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getThumbnail(): ?string
    {
        return $this->thumbnail;
    }

    public function setThumbnail(string $thumbnail): self
    {
        $this->thumbnail = $thumbnail;

        return $this;
    }

    public function getAuthor(): ?User
    {
        return $this->author;
    }

    public function setAuthor(?User $author): self
    {
        $this->author = $author;

        return $this;
    }

    /**
     * @return Collection|LessonChapter[]
     */
    public function getLessonChapters(): Collection
    {
        return $this->lessonChapters;
    }

    public function addLessonChapter(LessonChapter $lessonChapter): self
    {
        if (!$this->lessonChapters->contains($lessonChapter)) {
            $this->lessonChapters[] = $lessonChapter;
            $lessonChapter->setLesson($this);
        }

        return $this;
    }

    public function removeLessonChapter(LessonChapter $lessonChapter): self
    {
        if ($this->lessonChapters->contains($lessonChapter)) {
            $this->lessonChapters->removeElement($lessonChapter);
            // set the owning side to null (unless already changed)
            if ($lessonChapter->getLesson() === $this) {
                $lessonChapter->setLesson(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Category[]
     */
    public function getCategories(): Collection
    {
        return $this->categories;
    }

    public function addCategory(Category $category): self
    {
        if (!$this->categories->contains($category)) {
            $this->categories[] = $category;
            $category->addLesson($this);
        }

        return $this;
    }

    public function removeCategory(Category $category): self
    {
        if ($this->categories->contains($category)) {
            $this->categories->removeElement($category);
            $category->removeLesson($this);
        }

        return $this;
    }

    /**
     * @return Collection|LessonHistory[]
     */
    public function getLessonHistories(): Collection
    {
        return $this->lessonHistories;
    }

    public function addLessonHistory(LessonHistory $lessonHistory): self
    {
        if (!$this->lessonHistories->contains($lessonHistory)) {
            $this->lessonHistories[] = $lessonHistory;
            $lessonHistory->setLesson($this);
        }

        return $this;
    }

    public function removeLessonHistory(LessonHistory $lessonHistory): self
    {
        if ($this->lessonHistories->contains($lessonHistory)) {
            $this->lessonHistories->removeElement($lessonHistory);
            // set the owning side to null (unless already changed)
            if ($lessonHistory->getLesson() === $this) {
                $lessonHistory->setLesson(null);
            }
        }

        return $this;
    }

    public function getEvaluation(): ?Evaluation
    {
        return $this->evaluation;
    }

    public function setEvaluation(Evaluation $evaluation): self
    {
        $this->evaluation = $evaluation;

        // set the owning side of the relation if necessary
        if ($evaluation->getLesson() !== $this) {
            $evaluation->setLesson($this);
        }

        return $this;
    }

}
