<?php


namespace App\Entity\Backend\Contact;


use App\Entity\TimestampEntity;
use App\Repository\ContactFormRepository;
use App\Service\Backend\Form\GenerateFormInputInterface;
use App\Service\Backend\Form\GenerateFormInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use phpDocumentor\Reflection\Types\This;

/**
 * Class ContactForm
 * @package App\Entity\Backend\Contact
 * @ORM\HasLifecycleCallbacks()
 * @ORM\Entity(repositoryClass=ContactFormRepository::class)
 */
class ContactForm implements GenerateFormInterface
{
    use TimestampEntity;

    /**
     * @var int
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @ORM\Column(name="id", type="integer")
     */
    private int $id;

    /**
     * @var string
     * @ORM\Column(name="name", type="string")
     */
    private string $name;

    /**
     * @var string
     * @ORM\Column(name="path", type="string")
     */
    private string $path;

    /**
     * @var bool
     * @ORM\Column(name="enabled", type="boolean")
     */
    private bool $enabled;

    /**
     * @ORM\OrderBy({"positionElement" = "ASC"})
     * @ORM\OneToMany(targetEntity=ContactInput::class, mappedBy="contactForm", fetch="EAGER", cascade={"persist", "remove"})
     */
    private $fields;

    /**
     * @var GenerateFormInterface
     * @ORM\OneToMany(targetEntity="App\Entity\Backend\Contact\ContactObject", mappedBy="form")
     */
    private $contact;

    public function __construct()
    {
        $this->fields = new ArrayCollection();
        $this->contact = new ArrayCollection();
    }

    /**
     * @param int $id
     * @return ContactForm
     */
    public function setId(int $id): ContactForm
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return ContactForm
     */
    public function setName(string $name): ContactForm
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function getPath(): string
    {
        return $this->path;
    }

    /**
     * @param string $path
     * @return ContactForm
     */
    public function setPath(string $path): ContactForm
    {
        $this->path = $path;
        return $this;
    }

    /**
     * @return bool
     */
    public function isEnabled(): bool
    {
        return $this->enabled;
    }

    /**
     * @param bool $enabled
     * @return ContactForm
     */
    public function setEnabled(bool $enabled = false): ContactForm
    {
        $this->enabled = $enabled;
        return $this;
    }

    /**
     * @return Collection|ContactInput[]
     */
    public function getFields(): Collection
    {
        return $this->fields;
    }

    /**
     * @param array $fields
     * @return ContactForm
     */
    public function setFields(array $fields = null): ContactForm
    {
        $this->fields = $fields ?? new ArrayCollection();
        return $this;
    }

    public function addField(GenerateFormInputInterface $contactInput): self
    {
        if (!$this->fields->contains($contactInput)) {
            $this->fields[] = $contactInput;
            $contactInput->setContactForm($this);
        }

        return $this;
    }

    public function removeField(ContactInput $contactInput): self
    {
        if ($this->fields->contains($contactInput)) {
            $this->fields->removeElement($contactInput);
            // set the owning side to null (unless already changed)
            if ($contactInput->getContactForm() === $this) {
                $contactInput->setContactForm(null);
            }
        }

        return $this;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEnabled(): ?bool
    {
        return $this->enabled;
    }

    /**
     * @return Collection|ContactObject[]
     */
    public function getContact(): Collection
    {
        return $this->contact;
    }

    public function addContact(ContactObject $contact): self
    {
        if (!$this->contact->contains($contact)) {
            $this->contact[] = $contact;
            $contact->setForm($this);
        }

        return $this;
    }

    public function removeContact(ContactObject $contact): self
    {
        if ($this->contact->contains($contact)) {
            $this->contact->removeElement($contact);
            // set the owning side to null (unless already changed)
            if ($contact->getForm() === $this) {
                $contact->setForm(null);
            }
        }

        return $this;
    }

}
