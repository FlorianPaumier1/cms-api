<?php


namespace App\Entity\Backend\Contact;

use App\Entity\Backend\Evaluation\EvaluationObject;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class ContactMeta
 * @package App\Entity\Backend\Contact
 * @ORM\Entity()
 * @ORM\HasLifecycleCallbacks()
 */
class ContactMeta
{

    /**
     * @ORM\Id()
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @var int
     */
    private int $id;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Backend\Contact\ContactInput")
     * @var ContactInput
     */
    private $input;

    /**
     * @ORM\Column(type="string")
     * @var string
     */
    private $value;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Backend\Contact\ContactObject", inversedBy="inputs", cascade={"persist"})
     * @var ContactObject
     */
    private $contact;

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getValue(): ?string
    {
        return $this->value;
    }

    public function setValue(string $value): self
    {
        $this->value = $value;

        return $this;
    }

    public function getInput(): ?ContactInput
    {
        return $this->input;
    }

    public function setInput(?ContactInput $input): self
    {
        $this->input = $input;

        return $this;
    }

    public function getContact(): ?ContactObject
    {
        return $this->contact;
    }

    public function setContact(?ContactObject $contact): self
    {
        $this->contact = $contact;

        return $this;
    }

}
