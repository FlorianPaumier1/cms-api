<?php


namespace App\Entity\Backend\Contact;


use App\Service\Backend\Form\GenerateFormInputInterface;
use App\Repository\ContactInputRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class ContactInput
 * @package App\Entity\Backend\Contact
 * @ORM\HasLifecycleCallbacks()
 * @ORM\Entity(repositoryClass=ContactInputRepository::class)
 */
class ContactInput implements GenerateFormInputInterface, \ArrayAccess
{
    /**
     * @var int
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @ORM\Column(name="id", type="integer")
     */
    private int $id;

    /**
     * @var string
     * @ORM\Column(type="string")
     */
    private string $slug;

    /**
     * @var string
     * @ORM\Column(type="string")
     */
    private string $label;

    /**
     * @var string
     * @ORM\Column(type="string")
     */
    private string $type;

    /**
     * @var boolean
     * @ORM\Column(type="boolean")
     */
    private bool $required;

    /**
     * @var string
     * @ORM\Column(type="string")
     */
    private string $placeholder;

    /**
     * @var string
     * @ORM\Column(type="text")
     */
    private string $choices;

    /**
     * @var int
     * @ORM\Column(type="integer")
     */
    private int $positionElement;

    /**
     * @ORM\ManyToOne(targetEntity=ContactForm::class, inversedBy="contactInputs", cascade={"persist"})
     * @ORM\JoinColumn(nullable=false)
     */
    private ?ContactForm $contactForm;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getSlug(): string
    {
        return $this->slug;
    }

    /**
     * @param string $slug
     * @return ContactInput
     */
    public function setSlug(string $slug): ContactInput
    {
        $this->slug = $slug;
        return $this;
    }

    /**
     * @return string
     */
    public function getLabel(): string
    {
        return $this->label;
    }

    /**
     * @param string $label
     * @return ContactInput
     */
    public function setLabel(string $label): ContactInput
    {
        $this->label = $label;
        return $this;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @param string $type
     * @return ContactInput
     */
    public function setType(string $type): ContactInput
    {
        $this->type = $type;
        return $this;
    }

    /**
     * @return bool
     */
    public function isRequired(): bool
    {
        return $this->required;
    }

    /**
     * @param bool $required
     * @return ContactInput
     */
    public function setRequired(bool $required): ContactInput
    {
        $this->required = $required;
        return $this;
    }

    /**
     * @return string
     */
    public function getPlaceholder(): string
    {
        return $this->placeholder;
    }

    /**
     * @param string $placeholder
     * @return ContactInput
     */
    public function setPlaceholder(string $placeholder): ContactInput
    {
        $this->placeholder = $placeholder;
        return $this;
    }

    /**
     * @return string
     */
    public function getChoices(): string
    {
        return $this->choices;
    }

    /**
     * @param string|null $choices
     * @return ContactInput
     */
    public function setChoices(?string $choices): ContactInput
    {
        $this->choices = $choices;
        return $this;
    }

    /**
     * @return int
     */
    public function getPositionElement(): int
    {
        return $this->positionElement;
    }

    /**
     * @param int $positionElement
     * @return ContactInput
     */
    public function setPositionElement(int $positionElement): ContactInput
    {
        $this->positionElement = $positionElement;
        return $this;
    }

    public function getContactForm(): ?ContactForm
    {
        return $this->contactForm;
    }

    public function setContactForm(?ContactForm $contactForm): self
    {
        $this->contactForm = $contactForm;

        return $this;
    }

    public function offsetExists($offset) {
        return isset($this->$offset);
    }

    public function offsetSet($offset, $value) {
        $this->$offset = $value;
    }

    public function offsetGet($offset) {
        return $this->$offset;
    }

    public function offsetUnset($offset) {
        $this->$offset = null;
    }
}
