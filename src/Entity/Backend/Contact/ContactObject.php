<?php


namespace App\Entity\Backend\Contact;


use App\Entity\Backend\Evaluation\EvaluationObject;
use App\Entity\TimestampEntity;
use App\Service\Backend\Form\GenerateFormInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use PhpParser\Node\Expr\Cast\Object_;

/**
 * Class ContactObject
 * @package App\Entity\Backend\Contact
 * @ORM\Entity()
 * @ORM\HasLifecycleCallbacks()
 */
class ContactObject
{
    use TimestampEntity;

    /**
     * @var int
     * @ORM\Id()
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private int $id;

    /**
     * @var GenerateFormInterface
     * @ORM\ManyToOne(targetEntity="App\Entity\Backend\Contact\ContactForm", inversedBy="contact", cascade={"persist"})
     */
    private $form;

    /**
     * @var ContactMeta
     * @ORM\OneToMany(targetEntity="App\Entity\Backend\Contact\ContactMeta", mappedBy="contact", cascade={"persist"})
     */
    private $inputs;

    public function __construct()
    {
        $this->inputs = new ArrayCollection();
    }

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getForm(): ?GenerateFormInterface
    {
        return $this->form;
    }

    public function setForm(?ContactForm $form): self
    {
        $this->form = $form;

        return $this;
    }

    /**
     * @return Collection|ContactMeta[]
     */
    public function getInputs(): Collection
    {
        return $this->inputs;
    }

    public function addInput(ContactMeta $input): self
    {
        if (!$this->inputs->contains($input)) {
            $this->inputs[] = $input;
            $input->setContact($this);
        }

        return $this;
    }

    public function removeInput(ContactMeta $input): self
    {
        if ($this->inputs->contains($input)) {
            $this->inputs->removeElement($input);
            // set the owning side to null (unless already changed)
            if ($input->getContact() === $this) {
                $input->setContact(null);
            }
        }

        return $this;
    }
}
