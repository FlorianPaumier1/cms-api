<?php

namespace App\Entity\Backend\Evaluation;

use App\Entity\TimestampEntity;
use App\Entity\User;
use App\Repository\Backend\EvaluationUserRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;
use JMS\Serializer\Annotation\MaxDepth;

/**
 * @Serializer\ExclusionPolicy("all")
 * @ORM\HasLifecycleCallbacks()
 * @ORM\Entity(repositoryClass=EvaluationUserRepository::class)
 */
class EvaluationUser
{

    use TimestampEntity;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="evaluationUsers")
     * @ORM\JoinColumn(nullable=false)
     */
    private $user;

    /**
     * @ORM\OneToMany(targetEntity=EvaluationAnswer::class, mappedBy="evaluationUser", orphanRemoval=true)
     */
    private $answers;

    /**
     * @MaxDepth(1)
     * @ORM\ManyToOne(targetEntity=Evaluation::class, inversedBy="evaluationUsers")
     * @ORM\JoinColumn(nullable=false)
     */
    private $lesson;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $rating;

    public function __construct()
    {
        $this->answers = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    /**
     * @return Collection|EvaluationAnswer[]
     */
    public function getAnswers(): Collection
    {
        return $this->answers;
    }

    public function addAnswer(EvaluationAnswer $answer): self
    {
        if (!$this->answers->contains($answer)) {
            $this->answers[] = $answer;
            $answer->setEvaluationUser($this);
        }

        return $this;
    }

    public function removeAnswer(EvaluationAnswer $answer): self
    {
        if ($this->answers->removeElement($answer)) {
            // set the owning side to null (unless already changed)
            if ($answer->getEvaluationUser() === $this) {
                $answer->setEvaluationUser(null);
            }
        }

        return $this;
    }

    public function getLesson(): ?Evaluation
    {
        return $this->lesson;
    }

    public function setLesson(?Evaluation $lesson): self
    {
        $this->lesson = $lesson;

        return $this;
    }

    public function getRating(): ?int
    {
        return $this->rating;
    }

    public function setRating(?int $rating): self
    {
        $this->rating = $rating;

        return $this;
    }
}
