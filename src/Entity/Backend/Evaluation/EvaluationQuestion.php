<?php

namespace App\Entity\Backend\Evaluation;

use App\Entity\Backend\Contact\ContactForm;
use App\Entity\TimestampEntity;
use App\Repository\Backend\EvaluationQuestionRepository;
use App\Service\Backend\Form\GenerateFormInputInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;

/**
 * @Serializer\ExclusionPolicy("all")
 * @ORM\HasLifecycleCallbacks()
 * @ORM\Entity(repositoryClass=EvaluationQuestionRepository::class)
 */
class EvaluationQuestion implements GenerateFormInputInterface
{

    use TimestampEntity;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private ?int $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private ?string $type;

    /**
     * @ORM\Column(type="array", nullable=true)
     */
    private ?array $choices = [];

    /**
     * @ORM\Column(type="string", length=255)
     */
    private ?string $label;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private ?string $placeholder;

    /**
     * @ORM\ManyToOne(targetEntity=Evaluation::class, inversedBy="evaluationQuestions")
     * @ORM\JoinColumn(nullable=false)
     */
    private ?Evaluation $evaluation;

    /**
     * @ORM\OneToMany(targetEntity=EvaluationAnswer::class, mappedBy="question", orphanRemoval=true)
     */
    private Collection $evaluationAnswers;

    /**
     * @var string
     * @ORM\Column(type="string")
     */
    private string $slug;

    /**
     * @var boolean
     * @ORM\Column(type="boolean")
     */
    private bool $required;

    /**
     * @var int
     * @ORM\Column(type="integer")
     */
    private int $positionElement;

    /**
     * @return bool
     */
    public function isRequired(): bool
    {
        return $this->required;
    }

    /**
     * @param bool $required
     * @return EvaluationQuestion
     */
    public function setRequired(bool $required): EvaluationQuestion
    {
        $this->required = $required;
        return $this;
    }

    /**
     * @return int
     */
    public function getPositionElement(): int
    {
        return $this->positionElement;
    }

    /**
     * @param int $positionElement
     * @return EvaluationQuestion
     */
    public function setPositionElement(int $positionElement): EvaluationQuestion
    {
        $this->positionElement = $positionElement;
        return $this;
    }

    public function __construct()
    {
        $this->evaluationAnswers = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @return array|null
     */
    public function getChoices(): ?string
    {
        return $this->choices;
    }

    /**
     * @param array|null $choices
     * @return EvaluationQuestion
     */
    public function setChoices(?string $choices): EvaluationQuestion
    {
        $this->choices = $choices;
        return $this;
    }

    public function getLabel(): ?string
    {
        return $this->label;
    }

    public function setLabel(string $label): self
    {
        $this->label = $label;

        return $this;
    }

    public function getPlaceholder(): ?string
    {
        return $this->placeholder;
    }

    public function setPlaceholder(?string $placeholder): self
    {
        $this->placeholder = $placeholder;

        return $this;
    }

    public function getEvaluation(): ?Evaluation
    {
        return $this->evaluation;
    }

    public function setEvaluation(?Evaluation $evaluation): self
    {
        $this->evaluation = $evaluation;

        return $this;
    }

    /**
     * @return Collection|EvaluationAnswer[]
     */
    public function getEvaluationAnswers(): Collection
    {
        return $this->evaluationAnswers;
    }

    public function addEvaluationAnswer(EvaluationAnswer $evaluationAnswer): self
    {
        if (!$this->evaluationAnswers->contains($evaluationAnswer)) {
            $this->evaluationAnswers[] = $evaluationAnswer;
            $evaluationAnswer->setQuestion($this);
        }

        return $this;
    }

    public function removeEvaluationAnswer(EvaluationAnswer $evaluationAnswer): self
    {
        if ($this->evaluationAnswers->removeElement($evaluationAnswer)) {
            // set the owning side to null (unless already changed)
            if ($evaluationAnswer->getQuestion() === $this) {
                $evaluationAnswer->setQuestion(null);
            }
        }

        return $this;
    }

    /**
     * @return string
     */
    public function getSlug(): string
    {
        return $this->slug;
    }

    /**
     * @param string $slug
     */
    public function setSlug(string $slug): self
    {
        $this->slug = $slug;
        return $this;
    }
}
