<?php

namespace App\Entity\Backend\Evaluation;

use App\Entity\Lesson;
use App\Entity\TimestampEntity;
use App\Repository\Backend\EvaluationRepository;
use App\Service\Backend\Form\GenerateFormInputInterface;
use App\Service\Backend\Form\GenerateFormInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;
use JMS\Serializer\Annotation\MaxDepth;

/**
 * @Serializer\ExclusionPolicy("all")
 * @ORM\HasLifecycleCallbacks()
 * @ORM\Entity(repositoryClass=EvaluationRepository::class)
 */
class Evaluation implements GenerateFormInterface
{
    use TimestampEntity;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @var string
     * @ORM\Column(name="path", type="string")
     */
    private string $path;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $timeToDo;

    /**
     * @ORM\OneToMany(targetEntity=EvaluationQuestion::class, mappedBy="ManyToOne", orphanRemoval=true, cascade={"persist"})
     */
    private $fields;

    /**
     * @MaxDepth(1)
     * @ORM\OneToOne(targetEntity=Lesson::class, inversedBy="evaluation", cascade={"persist"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $lesson;

    /**
     * @ORM\OneToMany(targetEntity=EvaluationUser::class, mappedBy="lesson", orphanRemoval=true)
     */
    private $evaluationUsers;

    public function __construct()
    {
        $this->fields = new ArrayCollection();
        $this->evaluationUsers = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getTimeToDo(): ?int
    {
        return $this->timeToDo;
    }

    public function setTimeToDo(int $timeToDo): self
    {
        $this->timeToDo = $timeToDo;

        return $this;
    }

    /**
     * @return Collection|EvaluationQuestion[]
     */
    public function getFields(): Collection
    {
        return $this->fields;
    }

    public function addField(GenerateFormInputInterface $evaluationQuestion): self
    {
        if (!$this->fields->contains($evaluationQuestion)) {
            $this->fields[] = $evaluationQuestion;
            $evaluationQuestion->setEvaluation($this);
        }

        return $this;
    }

    public function removeField(EvaluationQuestion $evaluationQuestion): self
    {
        // set the owning side to null (unless already changed)
        if ($this->fields->removeElement($evaluationQuestion) && $evaluationQuestion->getEvaluation() === $this) {
            $evaluationQuestion->setEvaluation(null);
        }

        return $this;
    }

    public function setFields(array $fields = null)
    {
        $this->fields = $fields;
    }
    public function getLesson(): ?Lesson
    {
        return $this->lesson;
    }

    public function setLesson(Lesson $lesson): self
    {
        $this->lesson = $lesson;

        return $this;
    }

    /**
     * @return Collection|EvaluationUser[]
     */
    public function getEvaluationUsers(): Collection
    {
        return $this->evaluationUsers;
    }

    public function addEvaluationUser(EvaluationUser $evaluationUser): self
    {
        if (!$this->evaluationUsers->contains($evaluationUser)) {
            $this->evaluationUsers[] = $evaluationUser;
            $evaluationUser->setLesson($this);
        }

        return $this;
    }

    public function removeEvaluationUser(EvaluationUser $evaluationUser): self
    {
        // set the owning side to null (unless already changed)
        if ($this->evaluationUsers->removeElement($evaluationUser) && $evaluationUser->getLesson() === $this) {
            $evaluationUser->setLesson(null);
        }

        return $this;
    }


    /**
     * @return string
     */
    public function getPath(): string
    {
        return $this->path;
    }

    /**
     * @param string $path
     * @return Evaluation
     */
    public function setPath(string $path): Evaluation
    {
        $this->path = $path;
        return $this;
    }
}
