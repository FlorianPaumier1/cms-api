<?php

namespace App\Entity\Backend\Evaluation;

use App\Entity\TimestampEntity;
use App\Repository\Backend\EvaluationAnswerRepository;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;

/**
 * @Serializer\ExclusionPolicy("all")
 * @ORM\HasLifecycleCallbacks()
 * @ORM\Entity(repositoryClass=EvaluationAnswerRepository::class)
 */
class EvaluationAnswer
{
    use TimestampEntity;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $value;

    /**
     * @ORM\ManyToOne(targetEntity=EvaluationQuestion::class, inversedBy="evaluationAnswers")
     * @ORM\JoinColumn(nullable=false)
     */
    private $question;

    /**
     * @ORM\ManyToOne(targetEntity=EvaluationUser::class, inversedBy="answers")
     * @ORM\JoinColumn(nullable=false)
     */
    private $evaluationUser;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getValue(): ?string
    {
        return $this->value;
    }

    public function setValue(string $value): self
    {
        $this->value = $value;

        return $this;
    }

    public function getQuestion(): ?EvaluationQuestion
    {
        return $this->question;
    }

    public function setQuestion(?EvaluationQuestion $question): self
    {
        $this->question = $question;

        return $this;
    }

    public function getEvaluationUser(): ?EvaluationUser
    {
        return $this->evaluationUser;
    }

    public function setEvaluationUser(?EvaluationUser $evaluationUser): self
    {
        $this->evaluationUser = $evaluationUser;

        return $this;
    }
}
