<?php

namespace App\Entity;

use App\Entity\Backend\Evaluation\EvaluationUser;
use App\Repository\UserRepository;
use DateTimeInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Serializer\Annotation\MaxDepth;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @Serializer\AccessorOrder("custom", custom = {"id","email","username","isConfirmed", "createdAt"})
 * @Serializer\ExclusionPolicy("ALL")
 * @ORM\HasLifecycleCallbacks()
 * @ORM\Entity(repositoryClass=UserRepository::class)
 */
class User implements UserInterface
{

    use TimestampEntity;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Serializer\Expose()
     * @Serializer\Groups({"user_admin", "user_login"})
     */
    private ?int $id;

    /**
     * @Serializer\Expose()
     * @Serializer\Groups({"user", "user_light", "user_admin", "user_login"})
     * @Assert\NotBlank()
     * @Assert\Length(max="255")
     * @ORM\Column(type="string", length=180, unique=true)
     */
    private string $username;

    /**
     * @Serializer\Expose()
     * @Serializer\Groups({"user_admin", "user_login"})
     * @ORM\Column(type="json")
     */
    private array $roles = [];

    /**
     * @var string The hashed password
     * @ORM\Column(type="string")
     */
    private string $password;

    /**
     * @Serializer\Exclude()
     * @Assert\Length(
     *     min="4",
     *     minMessage="Votre mot de passe doit être d'au moins 4 caractère"
     *     )
     * @Assert\Regex(
     *     pattern="/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d\W+]{4,}$/",
     *      message="Votre mot de passe doit contenir au moins 1 majuscule, 1 minuscule, 1 chiffre et 1 caractère spécial"
     * )
     */
    private ?string $plainPassword;

    /**
     * @Serializer\Expose()
     * @Serializer\Groups({"user", "user_light", "user_admin"})
     * @Assert\Email()
     * @Assert\NotBlank()
     * @ORM\Column(type="string", length=255)
     */
    private ?string $email;

    /**
     * @Serializer\Expose()
     * @Serializer\Groups({"user_admin"})
     * @ORM\Column(type="boolean")
     */
    private bool $isConfirmed;

    /**
     * @Serializer\Expose()
     * @Serializer\Groups({"user_admin"})
     * @ORM\Column(type="datetime", nullable=true)
     */
    private ?DateTimeInterface $confirmedAt;

    /**
     * @Serializer\Expose()
     * @Serializer\Groups({"admin"})
     * @ORM\Column(type="boolean")
     */
    private bool $isEnabled;

    /**
     * @Serializer\Expose()
     * @Serializer\Groups({"user", "user_admin", "user_login"})
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private ?string $avatar;

    /**
     * @Serializer\Expose()
     * @Serializer\Groups({"user", "user_admin"})
     * @ORM\Column(type="text", nullable=true)
     */
    private ?string $description;

    /**
     * @Serializer\Expose()
     * @Serializer\Groups({"user", "user_light", "user_admin"})
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private ?string $firstName;

    /**
     * @Serializer\Expose()
     * @Serializer\Groups({"user", "user_light", "user_admin"})
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private ?string $lastName;

    /**
     * @Serializer\Exclude()
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private ?string $confirmationToken;

    /**
     * @Serializer\Exclude()
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private ?string $forgetPasswordToken;

    /**
     * @MaxDepth(1)
     * @Serializer\Groups({"user"})
     * @ORM\OneToMany(targetEntity=Lesson::class, mappedBy="author", orphanRemoval=true, fetch="LAZY")
     */
    private Collection $lessons;

    /**
     * @MaxDepth(1)
     * @Serializer\Groups({"user"})
     * @ORM\OneToMany(targetEntity=Article::class, mappedBy="author", fetch="LAZY")
     */
    private Collection $articles;

    /**
     * @MaxDepth(1)
     * @Serializer\Groups({"user"})
     * @ORM\OneToMany(targetEntity=LessonHistory::class, mappedBy="user", fetch="LAZY")
     */
    private Collection $lessonHistories;

    /**
     * @MaxDepth(1)
     * @Serializer\Groups({"user"})
     * @ORM\OneToMany(targetEntity=Event::class, mappedBy="author", fetch="LAZY")
     */
    private Collection $events;

    /**
     * @MaxDepth(1)
     * @Serializer\Groups({"user"})
     * @ORM\OneToMany(targetEntity=Contact::class, mappedBy="author", fetch="LAZY")
     */
    private Collection $contacts;

    /**
     * @MaxDepth(1)
     * @Serializer\Groups({"user"})
     * @ORM\OneToMany(targetEntity=SondageAnswer::class, mappedBy="user", fetch="LAZY")
     */
    private Collection $sondageAnswers;

    /**
     * @MaxDepth(1)
     * @Serializer\Groups({"user"})
     * @ORM\OneToMany(targetEntity=Faq::class, mappedBy="author", fetch="LAZY")
     */
    private Collection $faqs;

    /**
     * @MaxDepth(1)
     * @ORM\ManyToMany(targetEntity=ChatRoom::class, mappedBy="users", fetch="LAZY")
     */
    private Collection $chatRooms;

    /**
     * @MaxDepth(1)
     * @MaxDepth(1)
     * @ORM\OneToMany(targetEntity=EventInscription::class, mappedBy="user", orphanRemoval=true, fetch="LAZY")
     */
    private Collection $eventInscriptions;

    /**
     * @MaxDepth(1)
     * @ORM\OneToMany(targetEntity=Forum::class, mappedBy="author", fetch="LAZY")
     */
    private Collection $forums;

    /**
     * @MaxDepth(1)
     * @ORM\OneToMany(targetEntity=ForumAnswer::class, mappedBy="author", fetch="LAZY")
     */
    private Collection $forumAnswers;

    /**
     * @MaxDepth(1)
     * @Serializer\Expose()
     * @Serializer\Groups({"user_profile", "user_eval"})
     * @ORM\OneToMany(targetEntity=Backend\Evaluation\EvaluationUser::class, mappedBy="user", orphanRemoval=true, fetch="LAZY")
     */
    private Collection $evaluationUsers;

    public function __construct()
    {
        $this->roles = array('ROLE_USER');
        $this->isEnabled = false;
        $this->isConfirmed = false;
        $this->lessons = new ArrayCollection();
        $this->articles = new ArrayCollection();
        $this->lessonHistories = new ArrayCollection();
        $this->events = new ArrayCollection();
        $this->contacts = new ArrayCollection();
        $this->sondageAnswers = new ArrayCollection();
        $this->faqs = new ArrayCollection();
        $this->chatRooms = new ArrayCollection();
        $this->eventInscriptions = new ArrayCollection();
        $this->forums = new ArrayCollection();
        $this->forumAnswers = new ArrayCollection();
        $this->evaluationUsers = new ArrayCollection();
    }

    public function __serialize(): array
    {
        return [$this->username, $this->avatar, $this->roles];
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUsername(): string
    {
        return (string) $this->username;
    }

    public function setUsername(string $username): self
    {
        $this->username = $username;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getPlainPassword()
    {
        return $this->plainPassword;
    }

    /**
     * @param mixed $plainPassword
     */
    public function setPlainPassword($plainPassword): self
    {
        $this->password = password_hash($plainPassword, PASSWORD_BCRYPT);
        $this->plainPassword = $plainPassword;
        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getSalt()
    {
        // not needed when using the "bcrypt" algorithm in security.yaml
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getIsConfirmed(): ?bool
    {
        return $this->isConfirmed;
    }

    public function setIsConfirmed(bool $isConfirmed): self
    {
        $this->isConfirmed = $isConfirmed;

        return $this;
    }

    public function getConfirmedAt(): ?DateTimeInterface
    {
        return $this->confirmedAt;
    }

    public function setConfirmedAt(?DateTimeInterface $confirmedAt): self
    {
        $this->confirmedAt = $confirmedAt;

        return $this;
    }

    public function getIsEnabled(): ?bool
    {
        return $this->isEnabled;
    }

    public function setIsEnabled(bool $isEnabled): self
    {
        $this->isEnabled = $isEnabled;

        return $this;
    }

    public function getAvatar(): ?string
    {
        return $this->avatar;
    }

    public function setAvatar(?string $avatar): self
    {
        $this->avatar = $avatar;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    public function setFirstName(?string $firstName): self
    {
        $this->firstName = $firstName;

        return $this;
    }

    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    public function setLastName(?string $lastName): self
    {
        $this->lastName = $lastName;

        return $this;
    }

    public function getConfirmationToken(): ?string
    {
        return $this->confirmationToken;
    }

    public function setConfirmationToken(?string $confirmationToken): self
    {
        $this->confirmationToken = $confirmationToken;

        return $this;
    }

    public function getForgetPasswordToken(): ?string
    {
        return $this->forgetPasswordToken;
    }

    public function setForgetPasswordToken(?string $forgetPasswordToken): self
    {
        $this->forgetPasswordToken = $forgetPasswordToken;

        return $this;
    }

    /**
     * @return Collection|Lesson[]
     */
    public function getLessons(): Collection
    {
        return $this->lessons;
    }

    public function addLesson(Lesson $lesson): self
    {
        if (!$this->lessons->contains($lesson)) {
            $this->lessons[] = $lesson;
            $lesson->setAuthor($this);
        }

        return $this;
    }

    public function removeLesson(Lesson $lesson): self
    {
        if ($this->lessons->contains($lesson)) {
            $this->lessons->removeElement($lesson);
            // set the owning side to null (unless already changed)
            if ($lesson->getAuthor() === $this) {
                $lesson->setAuthor(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Article[]
     */
    public function getArticles(): Collection
    {
        return $this->articles;
    }

    public function addArticle(Article $article): self
    {
        if (!$this->articles->contains($article)) {
            $this->articles[] = $article;
            $article->setAuthor($this);
        }

        return $this;
    }

    public function removeArticle(Article $article): self
    {
        if ($this->articles->contains($article)) {
            $this->articles->removeElement($article);
            // set the owning side to null (unless already changed)
            if ($article->getAuthor() === $this) {
                $article->setAuthor(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|LessonHistory[]
     */
    public function getLessonHistories(): Collection
    {
        return $this->lessonHistories;
    }

    public function addLessonHistory(LessonHistory $lessonHistory): self
    {
        if (!$this->lessonHistories->contains($lessonHistory)) {
            $this->lessonHistories[] = $lessonHistory;
            $lessonHistory->setUser($this);
        }

        return $this;
    }

    public function removeLessonHistory(LessonHistory $lessonHistory): self
    {
        if ($this->lessonHistories->contains($lessonHistory)) {
            $this->lessonHistories->removeElement($lessonHistory);
            // set the owning side to null (unless already changed)
            if ($lessonHistory->getUser() === $this) {
                $lessonHistory->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Event[]
     */
    public function getEvents(): Collection
    {
        return $this->events;
    }

    public function addEvent(Event $event): self
    {
        if (!$this->events->contains($event)) {
            $this->events[] = $event;
            $event->setAuthor($this);
        }

        return $this;
    }

    public function removeEvent(Event $event): self
    {
        if ($this->events->contains($event)) {
            $this->events->removeElement($event);
            // set the owning side to null (unless already changed)
            if ($event->getAuthor() === $this) {
                $event->setAuthor(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Contact[]
     */
    public function getContacts(): Collection
    {
        return $this->contacts;
    }

    public function addContact(Contact $contact): self
    {
        if (!$this->contacts->contains($contact)) {
            $this->contacts[] = $contact;
            $contact->setAuthor($this);
        }

        return $this;
    }

    public function removeContact(Contact $contact): self
    {
        if ($this->contacts->contains($contact)) {
            $this->contacts->removeElement($contact);
            // set the owning side to null (unless already changed)
            if ($contact->getAuthor() === $this) {
                $contact->setAuthor(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|SondageAnswer[]
     */
    public function getSondageAnswers(): Collection
    {
        return $this->sondageAnswers;
    }

    public function addSondageAnswer(SondageAnswer $sondageAnswer): self
    {
        if (!$this->sondageAnswers->contains($sondageAnswer)) {
            $this->sondageAnswers[] = $sondageAnswer;
            $sondageAnswer->setUser($this);
        }

        return $this;
    }

    public function removeSondageAnswer(SondageAnswer $sondageAnswer): self
    {
        if ($this->sondageAnswers->contains($sondageAnswer)) {
            $this->sondageAnswers->removeElement($sondageAnswer);
            // set the owning side to null (unless already changed)
            if ($sondageAnswer->getUser() === $this) {
                $sondageAnswer->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Faq[]
     */
    public function getFaqs(): Collection
    {
        return $this->faqs;
    }

    public function addFaq(Faq $faq): self
    {
        if (!$this->faqs->contains($faq)) {
            $this->faqs[] = $faq;
            $faq->setAuthor($this);
        }

        return $this;
    }

    public function removeFaq(Faq $faq): self
    {
        if ($this->faqs->contains($faq)) {
            $this->faqs->removeElement($faq);
            // set the owning side to null (unless already changed)
            if ($faq->getAuthor() === $this) {
                $faq->setAuthor(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|ChatRoom[]
     */
    public function getChatRooms(): Collection
    {
        return $this->chatRooms;
    }

    public function addChatRoom(ChatRoom $chatRoom): self
    {
        if (!$this->chatRooms->contains($chatRoom)) {
            $this->chatRooms[] = $chatRoom;
            $chatRoom->addUser($this);
        }

        return $this;
    }

    public function removeChatRoom(ChatRoom $chatRoom): self
    {
        if ($this->chatRooms->contains($chatRoom)) {
            $this->chatRooms->removeElement($chatRoom);
            $chatRoom->removeUser($this);
        }

        return $this;
    }

    /**
     * @return Collection|EventInscription[]
     */
    public function getEventInscriptions(): Collection
    {
        return $this->eventInscriptions;
    }

    public function addEventInscription(EventInscription $eventInscription): self
    {
        if (!$this->eventInscriptions->contains($eventInscription)) {
            $this->eventInscriptions[] = $eventInscription;
            $eventInscription->setUser($this);
        }

        return $this;
    }

    public function removeEventInscription(EventInscription $eventInscription): self
    {
        if ($this->eventInscriptions->contains($eventInscription)) {
            $this->eventInscriptions->removeElement($eventInscription);
            // set the owning side to null (unless already changed)
            if ($eventInscription->getUser() === $this) {
                $eventInscription->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Forum[]
     */
    public function getForums(): Collection
    {
        return $this->forums;
    }

    public function addForum(Forum $forum): self
    {
        if (!$this->forums->contains($forum)) {
            $this->forums[] = $forum;
            $forum->setAuthor($this);
        }

        return $this;
    }

    public function removeForum(Forum $forum): self
    {
        if ($this->forums->contains($forum)) {
            $this->forums->removeElement($forum);
            // set the owning side to null (unless already changed)
            if ($forum->getAuthor() === $this) {
                $forum->setAuthor(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|ForumAnswer[]
     */
    public function getForumAnswers(): Collection
    {
        return $this->forumAnswers;
    }

    public function addForumAnswer(ForumAnswer $forumAnswer): self
    {
        if (!$this->forumAnswers->contains($forumAnswer)) {
            $this->forumAnswers[] = $forumAnswer;
            $forumAnswer->setAuthor($this);
        }

        return $this;
    }

    public function removeForumAnswer(ForumAnswer $forumAnswer): self
    {
        if ($this->forumAnswers->contains($forumAnswer)) {
            $this->forumAnswers->removeElement($forumAnswer);
            // set the owning side to null (unless already changed)
            if ($forumAnswer->getAuthor() === $this) {
                $forumAnswer->setAuthor(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|EvaluationUser[]
     */
    public function getEvaluationUsers(): Collection
    {
        return $this->evaluationUsers;
    }

    public function addEvaluationUser(EvaluationUser $evaluationUser): self
    {
        if (!$this->evaluationUsers->contains($evaluationUser)) {
            $this->evaluationUsers[] = $evaluationUser;
            $evaluationUser->setUser($this);
        }

        return $this;
    }

    public function removeEvaluationUser(EvaluationUser $evaluationUser): self
    {
        if ($this->evaluationUsers->removeElement($evaluationUser)) {
            // set the owning side to null (unless already changed)
            if ($evaluationUser->getUser() === $this) {
                $evaluationUser->setUser(null);
            }
        }

        return $this;
    }
}
