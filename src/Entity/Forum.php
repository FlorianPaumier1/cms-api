<?php

namespace App\Entity;

use App\Repository\ForumRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;

/**
 * @Serializer\ExclusionPolicy("all")
 * @ORM\Entity(repositoryClass=ForumRepository::class)
 * @ORM\HasLifecycleCallbacks()
 */
class Forum
{

    use TimestampEntity;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private ?int $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private ?string $title;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="forums")
     */
    private ?User $author;

    /**
     * @ORM\ManyToMany(targetEntity=Category::class)
     */
    private Collection $categories;

    /**
     * @ORM\Column(type="text")
     */
    private ?string $content;

    /**
     * @ORM\Column(type="boolean")
     */
    private ?bool $isVisible;

    /**
     * @ORM\Column(type="boolean")
     */
    private ?bool $isClosed;

    /**
     * @ORM\OneToMany(targetEntity=ForumAnswer::class, mappedBy="forum")
     */
    private $forumAnswers;

    public function __construct()
    {
        $this->categories = new ArrayCollection();
        $this->forumAnswers = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getAuthor(): ?User
    {
        return $this->author;
    }

    public function setAuthor(?User $author): self
    {
        $this->author = $author;

        return $this;
    }

    /**
     * @return Collection|Category[]
     */
    public function getCategories(): Collection
    {
        return $this->categories;
    }

    public function addCategory(Category $category): self
    {
        if (!$this->categories->contains($category)) {
            $this->categories[] = $category;
        }

        return $this;
    }

    public function removeCategory(Category $category): self
    {
        if ($this->categories->contains($category)) {
            $this->categories->removeElement($category);
        }

        return $this;
    }

    public function getContent(): ?string
    {
        return $this->content;
    }

    public function setContent(string $content): self
    {
        $this->content = $content;

        return $this;
    }

    public function getIsVisible(): ?bool
    {
        return $this->isVisible;
    }

    public function setIsVisible(bool $isVisible): self
    {
        $this->isVisible = $isVisible;

        return $this;
    }

    public function getIsClosed(): ?bool
    {
        return $this->isClosed;
    }

    public function setIsClosed(bool $isClosed): self
    {
        $this->isClosed = $isClosed;

        return $this;
    }

    /**
     * @return Collection|ForumAnswer[]
     */
    public function getForumAnswers(): Collection
    {
        return $this->forumAnswers;
    }

    public function addForumAnswer(ForumAnswer $forumAnswer): self
    {
        if (!$this->forumAnswers->contains($forumAnswer)) {
            $this->forumAnswers[] = $forumAnswer;
            $forumAnswer->setForum($this);
        }

        return $this;
    }

    public function removeForumAnswer(ForumAnswer $forumAnswer): self
    {
        if ($this->forumAnswers->contains($forumAnswer)) {
            $this->forumAnswers->removeElement($forumAnswer);
            // set the owning side to null (unless already changed)
            if ($forumAnswer->getForum() === $this) {
                $forumAnswer->setForum(null);
            }
        }

        return $this;
    }
}
