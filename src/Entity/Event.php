<?php

namespace App\Entity;

use App\Repository\EventRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;

/**
 * @JMS\AccessorOrder("custom", custom = {"id","title","slug", "author", "publishedAt", "takePlaceAt"})
 * @JMS\ExclusionPolicy("all")
 * @ORM\Entity(repositoryClass=EventRepository::class)
 * @ORM\HasLifecycleCallbacks()
 */
class Event
{
    /**
     * @JMS\Groups({"event_light", "event"})
     */
    use TimestampEntity;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @JMS\Expose
     * @JMS\Groups({"event_light", "event"})
     */
    private $id;

    /**
     * @JMS\Expose
     * @JMS\Groups({"event_light", "event"})
     * @ORM\Column(type="string", length=255)
     */
    private $title;

    /**
     * @JMS\Expose
     * @JMS\Groups({"event"})
     * @ORM\Column(type="text")
     */
    private $description;

    /**
     * @JMS\Expose
     * @JMS\Groups({"event", "user_light"})
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="events")
     */
    private $author;

    /**
     * @JMS\Expose
     * @JMS\Groups({"event"})
     * @ORM\Column(type="datetime")
     */
    private $publishedAt;

    /**
     * @JMS\Expose
     * @JMS\Groups({"event_light", "event"})
     * @ORM\Column(type="datetime")
     */
    private $takePlaceAt;

    /**
     * @JMS\Expose
     * @JMS\Groups({"event"})
     * @ORM\Column(type="string", length=255)
     */
    private $slug;

    /**
     * @JMS\Expose
     * @JMS\Groups({"event"})
     * @ORM\Column(type="boolean", options={"default":0}, nullable=true)
     */
    private $canSignUp;

    /**
     * @JMS\Expose
     * @JMS\Groups({"event_admin"})
     * @ORM\OneToMany(targetEntity=EventInscription::class, mappedBy="event", orphanRemoval=true)
     */
    private $eventInscriptions;

    public function __construct()
    {
        $this->eventInscriptions = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getAuthor(): ?User
    {
        return $this->author;
    }

    public function setAuthor(?User $author): self
    {
        $this->author = $author;

        return $this;
    }

    public function getPublishedAt(): ?\DateTimeInterface
    {
        return $this->publishedAt;
    }

    public function setPublishedAt(\DateTimeInterface $publishedAt): self
    {
        $this->publishedAt = $publishedAt;

        return $this;
    }

    public function getTakePlaceAt(): ?\DateTimeInterface
    {
        return $this->takePlaceAt;
    }

    public function setTakePlaceAt(\DateTimeInterface $takePlaceAt): self
    {
        $this->takePlaceAt = $takePlaceAt;

        return $this;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug(string $slug): self
    {
        $this->slug = $slug;

        return $this;
    }

    public function getCanSignUp(): ?bool
    {
        return $this->canSignUp;
    }

    public function setCanSignUp(bool $canSignUp): self
    {
        $this->canSignUp = $canSignUp;

        return $this;
    }

    /**
     * @return Collection|EventInscription[]
     */
    public function getEventInscriptions(): Collection
    {
        return $this->eventInscriptions;
    }

    public function addEventInscription(EventInscription $eventInscription): self
    {
        if (!$this->eventInscriptions->contains($eventInscription)) {
            $this->eventInscriptions[] = $eventInscription;
            $eventInscription->setEvent($this);
        }

        return $this;
    }

    public function removeEventInscription(EventInscription $eventInscription): self
    {
        if ($this->eventInscriptions->contains($eventInscription)) {
            $this->eventInscriptions->removeElement($eventInscription);
            // set the owning side to null (unless already changed)
            if ($eventInscription->getEvent() === $this) {
                $eventInscription->setEvent(null);
            }
        }

        return $this;
    }
}
