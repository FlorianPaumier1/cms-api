<?php

namespace App\Entity;

use App\Repository\LessonChapterRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;

/**
 * @Serializer\ExclusionPolicy("all")
 * @ORM\Entity(repositoryClass=LessonChapterRepository::class)
 * @ORM\HasLifecycleCallbacks()
 */
class LessonChapter
{
    use TimestampEntity;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @Serializer\Expose()
     * @Serializer\Groups({"chapter_light", "chapter"})
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @Serializer\MaxDepth(1)
     * @Serializer\Groups({"chapter"})
     * @Serializer\Expose()
     * @ORM\ManyToOne(targetEntity=Lesson::class, inversedBy="lessonChapters")
     * @ORM\JoinColumn(nullable=false)
     */
    private $lesson;

    /**
     * @Serializer\Expose()
     * @Serializer\Groups({"chapter_light", "chapter"})
     * @ORM\Column(type="string", length=255)
     */
    private $title;

    /**
     * @Serializer\Expose()
     * @Serializer\Groups({"chapter_light", "chapter"})
     * @ORM\Column(type="string", length=255)
     */
    private $slug;

    /**
     * @Serializer\Expose()
     * @Serializer\Groups({"chapter"})
     * @ORM\Column(type="integer")
     */
    private $position;

    /**
     * @Serializer\Expose()
     * @Serializer\Groups({"chapter_block", "chapter_block_light"})
     * @Serializer\MaxDepth(1)
     * @ORM\OrderBy({"position" = "ASC"})
     * @ORM\OneToMany(targetEntity=LessonChapterBlock::class, mappedBy="LessonChapter", orphanRemoval=true)
     */
    private $lessonChapterBlocks;

    public function __construct()
    {
        $this->lessonChapterBlocks = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLesson(): ?Lesson
    {
        return $this->lesson;
    }

    public function setLesson(?Lesson $lesson): self
    {
        $this->lesson = $lesson;

        return $this;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug(string $slug): self
    {
        $this->slug = $slug;

        return $this;
    }

    public function getPosition(): ?int
    {
        return $this->position;
    }

    public function setPosition(int $position): self
    {
        $this->position = $position;

        return $this;
    }

    /**
     * @return Collection|LessonChapterBlock[]
     */
    public function getLessonChapterBlocks(): Collection
    {
        return $this->lessonChapterBlocks;
    }

    public function addLessonChapterBlock(LessonChapterBlock $lessonChapterBlock): self
    {
        if (!$this->lessonChapterBlocks->contains($lessonChapterBlock)) {
            $this->lessonChapterBlocks[] = $lessonChapterBlock;
            $lessonChapterBlock->setLessonChapter($this);
        }

        return $this;
    }

    public function removeLessonChapterBlock(LessonChapterBlock $lessonChapterBlock): self
    {
        if ($this->lessonChapterBlocks->contains($lessonChapterBlock)) {
            $this->lessonChapterBlocks->removeElement($lessonChapterBlock);
            // set the owning side to null (unless already changed)
            if ($lessonChapterBlock->getLessonChapter() === $this) {
                $lessonChapterBlock->setLessonChapter(null);
            }
        }

        return $this;
    }
}
