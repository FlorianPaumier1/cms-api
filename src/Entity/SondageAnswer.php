<?php

namespace App\Entity;

use App\Repository\SondageAnswerRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=SondageAnswerRepository::class)
 * @ORM\HasLifecycleCallbacks()
 */
class SondageAnswer
{

    use TimestampEntity;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private ?int $id;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="sondageAnswers")
     * @ORM\JoinColumn(nullable=true)
     */
    private ?User $user;

    /**
     * @ORM\ManyToOne(targetEntity=SondageQuery::class, inversedBy="sondageAnswers")
     * @ORM\JoinColumn(nullable=false)
     */
    private ?SondageQuery $sonageQuery;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private ?string $ipAddress;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getSonageQuery(): ?SondageQuery
    {
        return $this->sonageQuery;
    }

    public function setSonageQuery(?SondageQuery $sonageQuery): self
    {
        $this->sonageQuery = $sonageQuery;

        return $this;
    }

    public function getIpAddress(): ?string
    {
        return $this->ipAddress;
    }

    public function setIpAddress(string $ipAddress): self
    {
        $this->ipAddress = $ipAddress;

        return $this;
    }

}
