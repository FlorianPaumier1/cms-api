<?php

namespace App\Entity;

use App\Repository\FaqAnswerRepository;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;

/**
 * @Serializer\AccessorOrder("custom", custom = {"id","query","answer","createdAt"})
 * @Serializer\ExclusionPolicy("all")
 * @ORM\Entity(repositoryClass=FaqAnswerRepository::class)
 * @ORM\HasLifecycleCallbacks()
 */
class FaqAnswer
{

    use TimestampEntity;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @Serializer\Expose()
     * @Serializer\Groups({"faq_answer"})
     * @ORM\Column(type="string", length=255)
     */
    private $query;

    /**
     * @Serializer\Expose()
     * @Serializer\Groups({"faq_answer"})
     * @ORM\Column(type="text")
     */
    private $answer;

    /**
     * @ORM\ManyToOne(targetEntity=Faq::class, inversedBy="faqAnswers")
     * @ORM\JoinColumn(nullable=false)
     */
    private $faq;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getQuery(): ?string
    {
        return $this->query;
    }

    public function setQuery(string $query): self
    {
        $this->query = $query;

        return $this;
    }

    public function getAnswer(): ?string
    {
        return $this->answer;
    }

    public function setAnswer(string $answer): self
    {
        $this->answer = $answer;

        return $this;
    }

    public function getFaq(): ?Faq
    {
        return $this->faq;
    }

    public function setFaq(?Faq $faq): self
    {
        $this->faq = $faq;

        return $this;
    }
}
