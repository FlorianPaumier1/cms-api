<?php

namespace App\Entity;

use App\Repository\FaqRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;
use JMS\Serializer\Annotation as Serializer;

/**
 * @JMS\AccessorOrder("custom", custom = {"id","title", "author", "isEnabled","category", "createdAt"})
 * @Serializer\ExclusionPolicy("all")
 * @ORM\Entity(repositoryClass=FaqRepository::class)
 * @ORM\HasLifecycleCallbacks()
 */
class Faq
{
    use TimestampEntity;

    /**
     * @JMS\Expose
     * @JMS\Groups({"faq_light", "faq"})
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @JMS\Expose
     * @JMS\Groups({"faq_light", "faq"})
     * @ORM\Column(type="string", length=255)
     */
    private $title;

    /**
     * @JMS\Expose
     * @JMS\Groups({"faq"})
     * @ORM\Column(type="text")
     */
    private $description;

    /**
     * @JMS\Expose
     * @JMS\Groups({"faq"})
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="faqs")
     * @ORM\JoinColumn(nullable=false)
     */
    private $author;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isEnabled;

    /**
     * @JMS\Expose
     * @JMS\Groups({"faq"})
     * @ORM\ManyToOne(targetEntity=Category::class, inversedBy="faqs")
     */
    private $category;

    /**
     * @JMS\Expose
     * @JMS\Groups({"faq", "faq_answer"})
     * @ORM\OneToMany(targetEntity=FaqAnswer::class, mappedBy="faq", orphanRemoval=true)
     */
    private $faqAnswers;

    public function __construct()
    {
        $this->faqAnswers = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getAuthor(): ?User
    {
        return $this->author;
    }

    public function setAuthor(?User $author): self
    {
        $this->author = $author;

        return $this;
    }

    public function getIsEnabled(): ?bool
    {
        return $this->isEnabled;
    }

    public function setIsEnabled(bool $isEnabled): self
    {
        $this->isEnabled = $isEnabled;

        return $this;
    }

    public function getCategory(): ?Category
    {
        return $this->category;
    }

    public function setCategory(?Category $category): self
    {
        $this->category = $category;

        return $this;
    }

    /**
     * @return Collection|FaqAnswer[]
     */
    public function getFaqAnswers(): Collection
    {
        return $this->faqAnswers;
    }

    public function addFaqAnswer(FaqAnswer $faqAnswer): self
    {
        if (!$this->faqAnswers->contains($faqAnswer)) {
            $this->faqAnswers[] = $faqAnswer;
            $faqAnswer->setFaq($this);
        }

        return $this;
    }

    public function removeFaqAnswer(FaqAnswer $faqAnswer): self
    {
        if ($this->faqAnswers->contains($faqAnswer)) {
            $this->faqAnswers->removeElement($faqAnswer);
            // set the owning side to null (unless already changed)
            if ($faqAnswer->getFaq() === $this) {
                $faqAnswer->setFaq(null);
            }
        }

        return $this;
    }
}
