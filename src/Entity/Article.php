<?php

namespace App\Entity;

use App\Repository\ArticleRepository;
use DateTimeImmutable;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;

/**
 * @JMS\AccessorOrder("custom", custom = {"id","title","slug", "author", "isVisible","category"})
 * @JMS\ExclusionPolicy("all")
 * @ORM\Entity(repositoryClass=ArticleRepository::class)
 * @ORM\HasLifecycleCallbacks
 */
class Article
{

    use TimestampEntity;

    /**
     * @ORM\Id()
     * @JMS\Expose()
     * @JMS\Groups({"article_light", "article_admin", "article"})
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private ?int $id;

    /**
     * @JMS\Expose()
     * @JMS\Groups({"article_light", "article_admin", "article"})
     * @ORM\Column(type="string", length=255)
     */
    private ?string $title;

    /**
     * @JMS\Expose()
     * @JMS\Groups({"article_light", "article_admin", "article"})
     * @ORM\Column(type="string", length=255)
     */
    private ?string $slug;

    /**
     * @JMS\Expose()
     * @JMS\Groups({"article_light", "article_admin", "article"})
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private ?string $thumbnail;

    /**
     * @JMS\Expose()
     * @JMS\Groups({"article_light", "article_admin", "article"})
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="articles")
     * @ORM\JoinColumn(nullable=false)
     */
    private ?User $author;

    /**
     * @JMS\Expose()
     * @JMS\Groups({"article_light", "article_admin"})
     * @ORM\Column(type="boolean")
     */
    private ?bool $isVisible;

    /**
     * @JMS\Expose()
     * @JMS\Groups({"article_admin", "article"})
     * @ORM\Column(type="text")
     */
    private ?string $content;

    /**
     * @JMS\Expose()
     * @JMS\Groups({"article_light", "article_admin", "article"})
     * @ORM\ManyToMany(targetEntity=Category::class, mappedBy="articles")
     */
    private ?Collection $categories;

    public function __construct()
    {
        $this->categories = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug(string $slug): self
    {
        $this->slug = $slug;

        return $this;
    }

    public function getThumbnail(): ?string
    {
        return $this->thumbnail;
    }

    public function setThumbnail(string $thumbnail): self
    {
        $this->thumbnail = $thumbnail;

        return $this;
    }

    public function getAuthor(): ?User
    {
        return $this->author;
    }

    public function setAuthor(?User $author): self
    {
        $this->author = $author;

        return $this;
    }

    public function getIsVisible(): ?bool
    {
        return $this->isVisible;
    }

    public function setIsVisible(bool $isVisible): self
    {
        $this->isVisible = $isVisible;

        return $this;
    }

    public function getContent(): ?string
    {
        return $this->content;
    }

    public function setContent(string $content): self
    {
        $this->content = $content;

        return $this;
    }

    /**
     * @return Collection|Category[]
     */
    public function getCategories(): Collection
    {
        return $this->categories;
    }

    public function addCategory(Category $category): self
    {
        if (!$this->categories->contains($category)) {
            $this->categories[] = $category;
            $category->addArticle($this);
        }

        return $this;
    }

    public function removeCategory(Category $category): self
    {
        if ($this->categories->contains($category)) {
            $this->categories->removeElement($category);
            $category->removeArticle($this);
        }

        return $this;
    }
}
