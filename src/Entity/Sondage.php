<?php

namespace App\Entity;

use App\Repository\SondageRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @JMS\AccessorOrder("custom", custom = {"id","title","createdAt"})
 * @JMS\ExclusionPolicy("all")
 * @ORM\Entity(repositoryClass=SondageRepository::class)
 * @ORM\HasLifecycleCallbacks()
 * @UniqueEntity("title")
 */
class Sondage
{

    use TimestampEntity;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private ?int $id;

    /**
     * @Assert\NotNull()
     * @JMS\Expose()
     * @JMS\Groups({"sondage_light", "sondage"})
     * @ORM\Column(type="string", length=255, unique=true)
     */
    private ?string $title;

    /**
     * @ORM\Column(type="boolean", options={"default":1})
     */
    private ?bool $isEnabled;

    /**
     * @JMS\Expose()
     * @JMS\Groups({"sondage"})
     * @ORM\OneToMany(targetEntity=SondageQuery::class, mappedBy="sondage", cascade={"persist", "remove"})
     */
    private Collection $sondageQueries;

    public function __construct()
    {
        $this->sondageQueries = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getIsEnabled(): ?bool
    {
        return $this->isEnabled;
    }

    public function setIsEnabled(bool $isEnabled): self
    {
        $this->isEnabled = $isEnabled;

        return $this;
    }

    /**
     * @return Collection
     */
    public function getSondageQueries(): Collection
    {
        return $this->sondageQueries;
    }

    public function addSondageQuery(SondageQuery $sondageQuery): self
    {
        if (!$this->sondageQueries->contains($sondageQuery)) {
            $this->sondageQueries[] = $sondageQuery;
            $sondageQuery->setSondage($this);
        }

        return $this;
    }

    public function removeSondageQuery(SondageQuery $sondageQuery): self
    {
        if ($this->sondageQueries->contains($sondageQuery)) {
            $this->sondageQueries->removeElement($sondageQuery);
            // set the owning side to null (unless already changed)
            if ($sondageQuery->getSondage() === $this) {
                $sondageQuery->setSondage(null);
            }
        }

        return $this;
    }
}
